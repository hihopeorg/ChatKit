package com.stfalcon.chatkit.sample.features.main;

import com.stfalcon.chatkit.dialogs.DialogsList;
import com.stfalcon.chatkit.messages.MessagesList;
import com.stfalcon.chatkit.sample.ResourceTable;
import com.stfalcon.chatkit.sample.features.demo.component.NinePatchDirectionalLayout;
import com.stfalcon.chatkit.sample.features.main.util.EventHelper;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.agp.components.*;
import ohos.agp.utils.Color;
import ohos.global.resource.NotExistException;
import ohos.global.resource.WrongTypeException;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;
import org.junit.Assert;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import java.io.IOException;
import java.lang.reflect.Field;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class MainAbilityTest {
    private static Ability ability = EventHelper.startAbility(MainAbility.class);
    static final HiLogLabel LABEL = new HiLogLabel(HiLog.LOG_APP, 0x01214, "ChatKitTest");

    private void stopThread(int x) {
        try {
            Thread.sleep(x);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void exec(String x) {
        try {
            Runtime.getRuntime().exec(x);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void test01View() {
        PageSlider pageSlider = (PageSlider) ability.findComponentById(ResourceTable.Id_pager);
        stopThread(2000);
        int currentPage = pageSlider.getCurrentPage();
        int count = pageSlider.getProvider().getCount();
        stopThread(2000);
        for (int i = 0; i < count; i++) {
            exec("input swipe 900 1200 100 1200 1000");
            stopThread(2000);
        }
        int currentPage1 = pageSlider.getCurrentPage();
        Assert.assertTrue("页面未全部加载", count == 5);
        Assert.assertTrue("页面滑动失败", currentPage != currentPage1);
        stopThread(2000);


    }

    @Test
    public void test02DefaultView() {
        stopThread(3000);
        PageSlider pageSlider = (PageSlider) ability.findComponentById(ResourceTable.Id_pager);
        stopThread(2000);
        int i = 0;
        do {
            if (pageSlider.getCurrentPage() < 0) {
                exec("input swipe 900 1200 100 1200 1000");
            } else {
                exec("input swipe 100 1200 900 1200 1000");
            }
            stopThread(2000);
            i++;
        } while (pageSlider.getCurrentPage() != 0 && i < 10);
        exec("input tap 540 1610 ");
        stopThread(2000);
        Ability currentTopAbility = AbilityDelegatorRegistry.getAbilityDelegator().getCurrentTopAbility();
        DialogsList dialogsList = (DialogsList) currentTopAbility.findComponentById(ResourceTable.Id_dialogsList);

        int childCount = dialogsList.getChildCount();
        DependentLayout dependentLayout = (DependentLayout) ((StackLayout) dialogsList.getComponentAt(0)).getComponentAt(0);
        Assert.assertTrue("消息列表未显示", childCount != 0 && dependentLayout != null);
        Image image = (Image) dependentLayout.findComponentById(ResourceTable.Id_dialogAvatar);
        Assert.assertNotNull("聊天对象头像未显示", image);
        exec("input swipe 540 2000 540 300 1000");
        stopThread(2000);
        AbilityDelegatorRegistry.getAbilityDelegator().triggerClickEvent(currentTopAbility, dependentLayout);
        stopThread(3000);
        Ability currentTopAbility1 = AbilityDelegatorRegistry.getAbilityDelegator().getCurrentTopAbility();
        MessagesList messagesList = (MessagesList) currentTopAbility1.findComponentById(ResourceTable.Id_messagesList);
        exec("input swipe 540 2000 540 300 1000");
        int outColor = -1;
        int inColor = -1;
        try {
            Field f = MessagesList.class.getDeclaredField("messagesListStyle");
            f.setAccessible(true);
            Object o = f.get(messagesList);
            Class<?> MessagesListStyleClazz = Class.forName("com.stfalcon.chatkit.messages.MessagesListStyle");
            Field field = MessagesListStyleClazz.getDeclaredField("outcomingDefaultBubblePressedColor");
            Field field1 = MessagesListStyleClazz.getDeclaredField("incomingDefaultBubblePressedColor");
            field.setAccessible(true);
            field1.setAccessible(true);
            outColor = (int) field.get(o);
            inColor = (int) field1.get(o);
        } catch (NoSuchFieldException | IllegalAccessException | ClassNotFoundException e) {
            HiLog.warn(LABEL, "xxx");
            e.printStackTrace();
        }
        Assert.assertTrue("聊天消息内容底色不为蓝色和白色", Integer.toHexString(outColor).substring(2).equals("4f62d7") && Integer.toHexString(inColor).substring(2).equals("efefef"));

        stopThread(3000);
        exec("input keyevent 4");
        stopThread(1000);
        exec("input keyevent 4");
        stopThread(1000);

    }

    @Test
    public void test03DefaultText() {
        stopThread(3000);
        PageSlider pageSlider = (PageSlider) ability.findComponentById(ResourceTable.Id_pager);
        stopThread(2000);
        int i = 0;
        do {
            if (pageSlider.getCurrentPage() < 0) {
                exec("input swipe 900 1200 100 1200 1000");
            } else {
                exec("input swipe 100 1200 900 1200 1000");
            }
            stopThread(2000);
            i++;
        } while (pageSlider.getCurrentPage() != 0 && i < 10);
        exec("input tap 540 1610 ");
        stopThread(2000);
        Ability currentTopAbility = AbilityDelegatorRegistry.getAbilityDelegator().getCurrentTopAbility();
        DialogsList dialogsList = (DialogsList) currentTopAbility.findComponentById(ResourceTable.Id_dialogsList);
        DependentLayout dependentLayout = (DependentLayout) ((StackLayout) dialogsList.getComponentAt(0)).getComponentAt(0);
        stopThread(2000);
        AbilityDelegatorRegistry.getAbilityDelegator().triggerClickEvent(currentTopAbility, dependentLayout);
        stopThread(3000);
        Ability currentTopAbility1 = AbilityDelegatorRegistry.getAbilityDelegator().getCurrentTopAbility();
        MessagesList messagesList = (MessagesList) currentTopAbility1.findComponentById(ResourceTable.Id_messagesList);
        int count = messagesList.getItemProvider().getCount();
        DependentLayout dependentLayout1 = (DependentLayout) messagesList.getComponentAt(0);

        DirectionalLayout directionalLayout = (DirectionalLayout) dependentLayout1.findComponentById(ResourceTable.Id_bubble);
        Text text = (Text) dependentLayout1.findComponentById(ResourceTable.Id_messageText);
        int width = directionalLayout.getLocationOnScreen()[0] + directionalLayout.getWidth() / 2;
        int height = directionalLayout.getLocationOnScreen()[1] + directionalLayout.getHeight() / 2;
        exec("input swipe " + width + " " + height + " " + width + " " + height + " 1500");
        stopThread(3000);
        Image copy = (Image) currentTopAbility1.findComponentById(ResourceTable.Id_action_copy);
        Image delete = (Image) currentTopAbility1.findComponentById(ResourceTable.Id_action_delete);

        HiLog.warn(LABEL, copy.getLocationOnScreen()[0] + ":" + copy.getLocationOnScreen()[1]);
        TextField textField = (TextField) currentTopAbility1.findComponentById(ResourceTable.Id_messageInput);
        AbilityDelegatorRegistry.getAbilityDelegator().triggerClickEvent(currentTopAbility1, copy);
        stopThread(3000);
        ability.getUITaskDispatcher().asyncDispatch(() -> {
            textField.setText(text.getText());
        });
        stopThread(2000);
        Image send = (Image) currentTopAbility1.findComponentById(ResourceTable.Id_messageSendButton);
        stopThread(1000);
        AbilityDelegatorRegistry.getAbilityDelegator().triggerClickEvent(currentTopAbility1, send);
        stopThread(2000);
        int count1 = messagesList.getItemProvider().getCount();
        Assert.assertTrue("发送失败", count + 1 == count1);

        int width1 = directionalLayout.getLocationOnScreen()[0] + directionalLayout.getWidth() / 2;
        int height1 = directionalLayout.getLocationOnScreen()[1] + directionalLayout.getHeight() / 2;
        exec("input swipe " + width1 + " " + height1 + " " + width1 + " " + height1 + " 1500");
        stopThread(2000);
        AbilityDelegatorRegistry.getAbilityDelegator().triggerClickEvent(currentTopAbility1, delete);
        stopThread(2000);
        int count2 = messagesList.getItemProvider().getCount();
        Assert.assertTrue("删除失败", count2 + 1 == count1);
        stopThread(3000);
        exec("input keyevent 4");
        stopThread(1000);
        exec("input keyevent 4");
        stopThread(1000);
    }

    @Test
    public void test04StyledView() {
        stopThread(3000);
        PageSlider pageSlider = (PageSlider) ability.findComponentById(ResourceTable.Id_pager);
        stopThread(2000);
        int i = 0;
        do {
            if (pageSlider.getCurrentPage() < 1) {
                exec("input swipe 900 1200 100 1200 1000");
            } else {
                exec("input swipe 100 1200 900 1200 1000");
            }
            stopThread(2000);
            i++;
        } while (pageSlider.getCurrentPage() != 1 && i < 10);
        exec("input tap 540 1610 ");
        stopThread(2000);
        Ability currentTopAbility = AbilityDelegatorRegistry.getAbilityDelegator().getCurrentTopAbility();
        stopThread(1000);
        DialogsList dialogsList = (DialogsList) currentTopAbility.findComponentById(ResourceTable.Id_dialogsList);

        int childCount = dialogsList.getChildCount();
        DependentLayout dependentLayout = (DependentLayout) ((StackLayout) dialogsList.getComponentAt(0)).getComponentAt(0);
        Assert.assertTrue("消息列表未显示", childCount != 0 && dependentLayout != null);
        Image image = (Image) dependentLayout.findComponentById(ResourceTable.Id_dialogAvatar);
        Assert.assertNotNull("聊天对象头像未显示", image);
        exec("input swipe 540 2000 540 300 1000");
        stopThread(2000);
        AbilityDelegatorRegistry.getAbilityDelegator().triggerClickEvent(currentTopAbility, dependentLayout);
        stopThread(3000);
        Ability currentTopAbility1 = AbilityDelegatorRegistry.getAbilityDelegator().getCurrentTopAbility();
        MessagesList messagesList = (MessagesList) currentTopAbility1.findComponentById(ResourceTable.Id_messagesList);
        exec("input swipe 540 2000 540 300 1000");
        int outColor = -1;
        int inColor = -1;
        try {
            Field f = MessagesList.class.getDeclaredField("messagesListStyle");
            f.setAccessible(true);
            Object o = f.get(messagesList);
            Class<?> MessagesListStyleClazz = Class.forName("com.stfalcon.chatkit.messages.MessagesListStyle");
            Field field = MessagesListStyleClazz.getDeclaredField("outcomingDefaultBubblePressedColor");
            Field field1 = MessagesListStyleClazz.getDeclaredField("incomingDefaultBubblePressedColor");
            field.setAccessible(true);
            field1.setAccessible(true);
            outColor = (int) field.get(o);
            inColor = (int) field1.get(o);
        } catch (NoSuchFieldException | IllegalAccessException | ClassNotFoundException e) {
            HiLog.warn(LABEL, "xxx");
            e.printStackTrace();
        }

        Color inColor1 = null;
        Color outColor1 = null;
        try {
            inColor1 = new Color(currentTopAbility1.getResourceManager().getElement(ResourceTable.Color_ivory_dark).getColor());
            outColor1 = new Color(currentTopAbility1.getResourceManager().getElement(ResourceTable.Color_green_dark).getColor());
        } catch (IOException | NotExistException | WrongTypeException e) {
            e.printStackTrace();
        }
        Assert.assertTrue("聊天消息内容底色不为绿色和淡橙色", inColor == inColor1.getValue() && outColor == outColor1.getValue());

        stopThread(3000);
        exec("input keyevent 4");
        stopThread(1000);
        exec("input keyevent 4");
        stopThread(1000);

    }

    @Test
    public void test05StyledText() {
        stopThread(3000);
        PageSlider pageSlider = (PageSlider) ability.findComponentById(ResourceTable.Id_pager);
        stopThread(2000);
        int i = 0;
        do {
            if (pageSlider.getCurrentPage() < 1) {
                exec("input swipe 900 1200 100 1200 1000");
            } else {
                exec("input swipe 100 1200 900 1200 1000");
            }
            stopThread(2000);
            i++;
        } while (pageSlider.getCurrentPage() != 1 && i < 10);
        exec("input tap 540 1610 ");
        stopThread(2000);
        Ability currentTopAbility = AbilityDelegatorRegistry.getAbilityDelegator().getCurrentTopAbility();
        stopThread(1000);
        DialogsList dialogsList = (DialogsList) currentTopAbility.findComponentById(ResourceTable.Id_dialogsList);
        DependentLayout dependentLayout = (DependentLayout) ((StackLayout) dialogsList.getComponentAt(0)).getComponentAt(0);
        stopThread(2000);
        AbilityDelegatorRegistry.getAbilityDelegator().triggerClickEvent(currentTopAbility, dependentLayout);
        stopThread(3000);
        Ability currentTopAbility1 = AbilityDelegatorRegistry.getAbilityDelegator().getCurrentTopAbility();
        MessagesList messagesList = (MessagesList) currentTopAbility1.findComponentById(ResourceTable.Id_messagesList);
        int count = messagesList.getItemProvider().getCount();
        DependentLayout dependentLayout1 = (DependentLayout) messagesList.getComponentAt(0);

        DirectionalLayout directionalLayout = (DirectionalLayout) dependentLayout1.findComponentById(ResourceTable.Id_bubble);
        Text text = (Text) dependentLayout1.findComponentById(ResourceTable.Id_messageText);
        int width = directionalLayout.getLocationOnScreen()[0] + directionalLayout.getWidth() / 2;
        int height = directionalLayout.getLocationOnScreen()[1] + directionalLayout.getHeight() / 2;
        exec("input swipe " + width + " " + height + " " + width + " " + height + " 1500");
        stopThread(3000);
        Image copy = (Image) currentTopAbility1.findComponentById(ResourceTable.Id_action_copy);
        Image delete = (Image) currentTopAbility1.findComponentById(ResourceTable.Id_action_delete);

        TextField textField = (TextField) currentTopAbility1.findComponentById(ResourceTable.Id_messageInput);
        AbilityDelegatorRegistry.getAbilityDelegator().triggerClickEvent(currentTopAbility1, copy);
        stopThread(3000);
        ability.getUITaskDispatcher().asyncDispatch(() -> {
            textField.setText(text.getText());
        });
        stopThread(2000);
        Image send = (Image) currentTopAbility1.findComponentById(ResourceTable.Id_messageSendButton);
        stopThread(1000);
        AbilityDelegatorRegistry.getAbilityDelegator().triggerClickEvent(currentTopAbility1, send);
        stopThread(2000);
        int count1 = messagesList.getItemProvider().getCount();
        Assert.assertTrue("发送失败", count + 1 == count1);

        int width1 = directionalLayout.getLocationOnScreen()[0] + directionalLayout.getWidth() / 2;
        int height1 = directionalLayout.getLocationOnScreen()[1] + directionalLayout.getHeight() / 2;
        exec("input swipe " + width1 + " " + height1 + " " + width1 + " " + height1 + " 1500");
        stopThread(2000);

        HiLog.warn(LABEL, delete.getLocationOnScreen()[0] + ":" + delete.getLocationOnScreen()[1]);
        AbilityDelegatorRegistry.getAbilityDelegator().triggerClickEvent(currentTopAbility1, delete);

        stopThread(2000);
        int count2 = messagesList.getItemProvider().getCount();
        Assert.assertTrue("删除失败", count2 + 1 == count1);
        stopThread(3000);
        exec("input keyevent 4");
        stopThread(1000);
        exec("input keyevent 4");
        stopThread(1000);

    }

    @Test
    public void test06CustomLayout() {
        stopThread(3000);
        PageSlider pageSlider = (PageSlider) ability.findComponentById(ResourceTable.Id_pager);
        stopThread(2000);
        int i = 0;
        do {
            if (pageSlider.getCurrentPage() < 2) {
                exec("input swipe 900 1200 100 1200 1000");
            } else {
                exec("input swipe 100 1200 900 1200 1000");
            }
            stopThread(2000);
            i++;
        } while (pageSlider.getCurrentPage() != 2 && i < 10);
        exec("input tap 540 1610 ");
        stopThread(2000);
        Ability currentTopAbility = AbilityDelegatorRegistry.getAbilityDelegator().getCurrentTopAbility();
        stopThread(1000);
        DialogsList dialogsList = (DialogsList) currentTopAbility.findComponentById(ResourceTable.Id_dialogsList);

        int childCount = dialogsList.getChildCount();
        DependentLayout dependentLayout = (DependentLayout) ((StackLayout) dialogsList.getComponentAt(0)).getComponentAt(0);
        Assert.assertTrue("消息列表未显示", childCount != 0 && dependentLayout != null);
        Image image = (Image) dependentLayout.findComponentById(ResourceTable.Id_dialogAvatar);
        Assert.assertNotNull("聊天对象头像未显示", image);
        exec("input swipe 540 2000 540 300 1000");
        stopThread(2000);
        AbilityDelegatorRegistry.getAbilityDelegator().triggerClickEvent(currentTopAbility, dependentLayout);
        stopThread(3000);
        Ability currentTopAbility1 = AbilityDelegatorRegistry.getAbilityDelegator().getCurrentTopAbility();
        MessagesList messagesList = (MessagesList) currentTopAbility1.findComponentById(ResourceTable.Id_messagesList);
        exec("input swipe 540 2000 540 300 1000");
        DependentLayout dependentLayout1 = (DependentLayout)messagesList.getComponentAt(0);
        NinePatchDirectionalLayout ninePatchDirectionalLayout = (NinePatchDirectionalLayout)dependentLayout1.findComponentById(ResourceTable.Id_bubble);

        stopThread(3000);
        exec("input keyevent 4");
        stopThread(1000);
        exec("input keyevent 4");
        stopThread(1000);

    }

    @Test
    public void test07CustomText() {
        stopThread(3000);
        PageSlider pageSlider = (PageSlider) ability.findComponentById(ResourceTable.Id_pager);
        stopThread(2000);
        int i = 0;
        do {
            if (pageSlider.getCurrentPage() < 2) {
                exec("input swipe 900 1200 100 1200 1000");
            } else {
                exec("input swipe 100 1200 900 1200 1000");
            }
            stopThread(2000);
            i++;
        } while (pageSlider.getCurrentPage() != 2 && i < 10);
        exec("input tap 540 1610 ");
        stopThread(2000);
        Ability currentTopAbility = AbilityDelegatorRegistry.getAbilityDelegator().getCurrentTopAbility();
        stopThread(1000);
        DialogsList dialogsList = (DialogsList) currentTopAbility.findComponentById(ResourceTable.Id_dialogsList);

        DependentLayout dependentLayout = (DependentLayout) ((StackLayout) dialogsList.getComponentAt(0)).getComponentAt(0);
        stopThread(2000);
        AbilityDelegatorRegistry.getAbilityDelegator().triggerClickEvent(currentTopAbility, dependentLayout);
        stopThread(3000);
        Ability currentTopAbility1 = AbilityDelegatorRegistry.getAbilityDelegator().getCurrentTopAbility();
        MessagesList messagesList = (MessagesList) currentTopAbility1.findComponentById(ResourceTable.Id_messagesList);
        int count = messagesList.getItemProvider().getCount();
        DependentLayout dependentLayout1 = (DependentLayout) messagesList.getComponentAt(0);

        Text text = (Text) dependentLayout1.findComponentById(ResourceTable.Id_messageText);

        TextField textField = (TextField) currentTopAbility1.findComponentById(ResourceTable.Id_messageInput);
        stopThread(3000);
        ability.getUITaskDispatcher().asyncDispatch(() -> {
            textField.setText(text.getText());
        });
        stopThread(2000);
        Image send = (Image) currentTopAbility1.findComponentById(ResourceTable.Id_messageSendButton);
        stopThread(1000);
        AbilityDelegatorRegistry.getAbilityDelegator().triggerClickEvent(currentTopAbility1, send);
        stopThread(2000);
        int count1 = messagesList.getItemProvider().getCount();
        Assert.assertTrue("发送失败", count + 1 == count1);

        stopThread(3000);
        exec("input keyevent 4");
        stopThread(1000);
        exec("input keyevent 4");
        stopThread(1000);

    }

    @Test
    public void test08CustomHolder() {
        stopThread(3000);
        PageSlider pageSlider = (PageSlider) ability.findComponentById(ResourceTable.Id_pager);
        stopThread(2000);
        int i = 0;
        do {
            if (pageSlider.getCurrentPage() < 3) {
                exec("input swipe 900 1200 100 1200 1000");
            } else {
                exec("input swipe 100 1200 900 1200 1000");
            }
            stopThread(2000);
            i++;
        } while (pageSlider.getCurrentPage() != 3 && i < 10);
        exec("input tap 540 1650 ");
        stopThread(2000);
        Ability currentTopAbility = AbilityDelegatorRegistry.getAbilityDelegator().getCurrentTopAbility();
        stopThread(1000);
        DialogsList dialogsList = (DialogsList) currentTopAbility.findComponentById(ResourceTable.Id_dialogsList);

        int childCount = dialogsList.getChildCount();
        DependentLayout dependentLayout = (DependentLayout) ((StackLayout) dialogsList.getComponentAt(0)).getComponentAt(0);
        Assert.assertTrue("消息列表未显示", childCount != 0 && dependentLayout != null);
        Image image = (Image) dependentLayout.findComponentById(ResourceTable.Id_dialogAvatar);
        Assert.assertNotNull("聊天对象头像未显示", image);
        exec("input swipe 540 2000 540 300 1000");
        stopThread(2000);
        AbilityDelegatorRegistry.getAbilityDelegator().triggerClickEvent(currentTopAbility, dependentLayout);
        stopThread(3000);
        Ability currentTopAbility1 = AbilityDelegatorRegistry.getAbilityDelegator().getCurrentTopAbility();
        MessagesList messagesList = (MessagesList) currentTopAbility1.findComponentById(ResourceTable.Id_messagesList);
        exec("input swipe 540 2000 540 300 1000");
        DependentLayout dependentLayout1 = (DependentLayout)messagesList.getComponentAt(0);
        NinePatchDirectionalLayout ninePatchDirectionalLayout = (NinePatchDirectionalLayout)dependentLayout1.findComponentById(ResourceTable.Id_bubble);

        stopThread(3000);
        exec("input keyevent 4");
        stopThread(1000);
        exec("input keyevent 4");
        stopThread(1000);
    }

    @Test
    public void test09CustomText() {
        stopThread(3000);
        PageSlider pageSlider = (PageSlider) ability.findComponentById(ResourceTable.Id_pager);
        stopThread(2000);
        int i = 0;
        do {
            if (pageSlider.getCurrentPage() < 3) {
                exec("input swipe 900 1200 100 1200 1000");
            } else {
                exec("input swipe 100 1200 900 1200 1000");
            }
            stopThread(2000);
            i++;
        } while (pageSlider.getCurrentPage() != 3 && i < 10);
        stopThread(2000);
        exec("input tap 540 1650 ");
        stopThread(3000);
        Ability currentTopAbility = AbilityDelegatorRegistry.getAbilityDelegator().getCurrentTopAbility();
        stopThread(1000);
        DialogsList dialogsList = (DialogsList) currentTopAbility.findComponentById(ResourceTable.Id_dialogsList);
        DependentLayout dependentLayout = (DependentLayout) ((StackLayout) dialogsList.getComponentAt(0)).getComponentAt(0);
        stopThread(2000);
        AbilityDelegatorRegistry.getAbilityDelegator().triggerClickEvent(currentTopAbility, dependentLayout);
        stopThread(3000);
        Ability currentTopAbility1 = AbilityDelegatorRegistry.getAbilityDelegator().getCurrentTopAbility();
        MessagesList messagesList = (MessagesList) currentTopAbility1.findComponentById(ResourceTable.Id_messagesList);
        int count = messagesList.getItemProvider().getCount();
        DependentLayout dependentLayout1 = (DependentLayout) messagesList.getComponentAt(0);

        Text text = (Text) dependentLayout1.findComponentById(ResourceTable.Id_messageText);

        TextField textField = (TextField) currentTopAbility1.findComponentById(ResourceTable.Id_messageInput);
        stopThread(3000);
        ability.getUITaskDispatcher().asyncDispatch(() -> {
            textField.setText(text.getText());
        });
        stopThread(2000);
        Image send = (Image) currentTopAbility1.findComponentById(ResourceTable.Id_messageSendButton);
        stopThread(1000);
        AbilityDelegatorRegistry.getAbilityDelegator().triggerClickEvent(currentTopAbility1, send);
        stopThread(2000);
        int count1 = messagesList.getItemProvider().getCount();
        Assert.assertTrue("发送失败", count + 1 == count1);

        stopThread(3000);
        exec("input keyevent 4");
        stopThread(1000);
        exec("input keyevent 4");
        stopThread(1000);
    }

    @Test
    public void test10CustomType() {
        stopThread(3000);
        PageSlider pageSlider = (PageSlider) ability.findComponentById(ResourceTable.Id_pager);
        stopThread(2000);
        int i = 0;
        do {
            if (pageSlider.getCurrentPage() < 4) {
                exec("input swipe 900 1200 100 1200 1000");
            } else {
                exec("input swipe 100 1200 900 1200 1000");
            }
            stopThread(2000);
            i++;
        } while (pageSlider.getCurrentPage() != 4 && i < 10);
        exec("input tap 540 1610 ");
        stopThread(3000);
        Ability currentTopAbility1 = AbilityDelegatorRegistry.getAbilityDelegator().getCurrentTopAbility();
        MessagesList messagesList = (MessagesList) currentTopAbility1.findComponentById(ResourceTable.Id_messagesList);
        exec("input swipe 540 2000 540 300 1000");
        int outColor = -1;
        int inColor = -1;
        try {
            Field f = MessagesList.class.getDeclaredField("messagesListStyle");
            f.setAccessible(true);
            Object o = f.get(messagesList);
            Class<?> MessagesListStyleClazz = Class.forName("com.stfalcon.chatkit.messages.MessagesListStyle");


            Field field = MessagesListStyleClazz.getDeclaredField("outcomingDefaultBubblePressedColor");
            Field field1 = MessagesListStyleClazz.getDeclaredField("incomingDefaultBubblePressedColor");
            field.setAccessible(true);
            field1.setAccessible(true);
            outColor = (int) field.get(o);
            inColor = (int) field1.get(o);
        } catch (Exception e) {
            HiLog.warn(LABEL, "xxx");
            e.printStackTrace();
        }


        Color inColor1 = null;
        Color outColor1 = null;
        try {
            inColor1 = new Color(currentTopAbility1.getResourceManager().getElement(com.stfalcon.chatkit.ResourceTable.Color_white_two).getColor());
            outColor1 = new Color(currentTopAbility1.getResourceManager().getElement(com.stfalcon.chatkit.ResourceTable.Color_cornflower_blue_two).getColor());
        } catch (IOException | NotExistException | WrongTypeException e) {
            e.printStackTrace();
        }
        Assert.assertTrue("聊天消息内容底色不为蓝色和灰色", inColor == inColor1.getValue() && outColor == outColor1.getValue());

        stopThread(3000);
        exec("input keyevent 4");
        stopThread(1000);
    }

    @Test
    public void test11CustomText() {
        stopThread(3000);
        PageSlider pageSlider = (PageSlider) ability.findComponentById(ResourceTable.Id_pager);
        stopThread(2000);
        int i = 0;
        do {
            if (pageSlider.getCurrentPage() < 4) {
                exec("input swipe 900 1200 100 1200 1000");
            } else {
                exec("input swipe 100 1200 900 1200 1000");
            }
            stopThread(2000);
            i++;
        } while (pageSlider.getCurrentPage() != 4 && i < 10);
        stopThread(2000);
        exec("input tap 540 1610 ");
        stopThread(3000);
        Ability currentTopAbility1 = AbilityDelegatorRegistry.getAbilityDelegator().getCurrentTopAbility();
        stopThread(1000);
        MessagesList messagesList = (MessagesList) currentTopAbility1.findComponentById(ResourceTable.Id_messagesList);
        DependentLayout dependentLayout1 = (DependentLayout) messagesList.getComponentAt(0);
        stopThread(1000);
        Text text = (Text) dependentLayout1.findComponentById(ResourceTable.Id_messageText);

        TextField textField = (TextField) currentTopAbility1.findComponentById(ResourceTable.Id_messageInput);
        stopThread(3000);
        ability.getUITaskDispatcher().asyncDispatch(() -> {
            textField.setText(text.getText());
        });
        stopThread(2000);
        Image send = (Image) currentTopAbility1.findComponentById(ResourceTable.Id_messageSendButton);
        int count = messagesList.getItemProvider().getCount();
        stopThread(1000);
        AbilityDelegatorRegistry.getAbilityDelegator().triggerClickEvent(currentTopAbility1, send);
        stopThread(2000);
        int count1 = messagesList.getItemProvider().getCount();
        Assert.assertTrue("发送失败" + count + ":" + count1, count + 1 == count1);

        stopThread(3000);
        exec("input keyevent 4");
        stopThread(1000);
    }

}