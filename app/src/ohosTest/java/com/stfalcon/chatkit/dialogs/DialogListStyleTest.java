package com.stfalcon.chatkit.dialogs;

import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.agp.components.Attr;
import ohos.agp.components.AttrSet;
import ohos.app.Context;
import org.junit.Test;

import java.util.Optional;

import static org.junit.Assert.*;
public class DialogListStyleTest {
    public static Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
    private DialogListStyle dialogListStyle = DialogListStyle.parse(context, new AttrSet() {
        @Override
        public Optional<String> getStyle() {
            return Optional.empty();
        }

        @Override
        public int getLength() {
            return 0;
        }

        @Override
        public Optional<Attr> getAttr(int i) {
            return Optional.empty();
        }

        @Override
        public Optional<Attr> getAttr(String s) {
            return Optional.empty();
        }
    });
    @Test
    public void getDialogTitleTextColor() {
        assertEquals(-14144981,dialogListStyle.getDialogTitleTextColor());
    }

    @Test
    public void getDialogTitleTextSize() {
        assertEquals(54,dialogListStyle.getDialogTitleTextSize());
    }

    @Test
    public void getDialogTitleTextStyle() {
        assertEquals(0,dialogListStyle.getDialogTitleTextStyle());
    }

    @Test
    public void getDialogUnreadTitleTextColor() {
        assertEquals(-14144981,dialogListStyle.getDialogUnreadTitleTextColor());
    }

    @Test
    public void getDialogUnreadTitleTextStyle() {
        assertEquals(0,dialogListStyle.getDialogUnreadTitleTextStyle());
    }

    @Test
    public void getDialogMessageTextColor() {
        assertEquals(-8421505,dialogListStyle.getDialogMessageTextColor());
    }

    @Test
    public void getDialogMessageTextSize() {
        assertEquals(48,dialogListStyle.getDialogMessageTextSize());
    }

    @Test
    public void getDialogMessageTextStyle() {
        assertEquals(0,dialogListStyle.getDialogMessageTextStyle());
    }

    @Test
    public void getDialogUnreadMessageTextColor() {
        assertEquals(-8421505,dialogListStyle.getDialogUnreadMessageTextColor());
    }

    @Test
    public void getDialogUnreadMessageTextStyle() {
        assertEquals(0,dialogListStyle.getDialogUnreadMessageTextStyle());
    }

    @Test
    public void getDialogDateColor() {
        assertEquals(-6513508,dialogListStyle.getDialogDateColor());
    }

    @Test
    public void getDialogDateSize() {
        assertEquals(42,dialogListStyle.getDialogDateSize());
    }

    @Test
    public void getDialogDateStyle() {
        assertEquals(0,dialogListStyle.getDialogDateStyle());
    }

    @Test
    public void getDialogUnreadDateColor() {
        assertEquals(-6513508,dialogListStyle.getDialogUnreadDateColor());
    }

    @Test
    public void getDialogUnreadDateStyle() {
        assertEquals(0,dialogListStyle.getDialogUnreadDateStyle());
    }

    @Test
    public void isDialogUnreadBubbleEnabled() {
        assertEquals(true,dialogListStyle.isDialogUnreadBubbleEnabled());
    }

    @Test
    public void getDialogUnreadBubbleTextColor() {
        assertEquals(-1,dialogListStyle.getDialogUnreadBubbleTextColor());
    }

    @Test
    public void getDialogUnreadBubbleTextSize() {
        assertEquals(39,dialogListStyle.getDialogUnreadBubbleTextSize());
    }

    @Test
    public void getDialogUnreadBubbleTextStyle() {
        assertEquals(0,dialogListStyle.getDialogUnreadBubbleTextStyle());
    }

    @Test
    public void getDialogUnreadBubbleBackgroundColor() {
        assertEquals(-11419556,dialogListStyle.getDialogUnreadBubbleBackgroundColor());
    }

    @Test
    public void getDialogAvatarWidth() {
        assertEquals(168,dialogListStyle.getDialogAvatarWidth());
    }

    @Test
    public void getDialogAvatarHeight() {
        assertEquals(168,dialogListStyle.getDialogAvatarHeight());
    }

    @Test
    public void isDialogDividerEnabled() {
        assertEquals(true,dialogListStyle.isDialogDividerEnabled());
    }

    @Test
    public void getDialogDividerColor() {
        assertEquals(-1,dialogListStyle.getDialogDividerColor());
    }

    @Test
    public void getDialogDividerLeftPadding() {
        assertEquals(264,dialogListStyle.getDialogDividerLeftPadding());
    }

    @Test
    public void getDialogDividerRightPadding() {
        assertEquals(0,dialogListStyle.getDialogDividerRightPadding());
    }

    @Test
    public void getDialogItemBackground() {
        assertEquals(0,dialogListStyle.getDialogItemBackground());
    }

    @Test
    public void getDialogUnreadItemBackground() {
        assertEquals(0,dialogListStyle.getDialogUnreadItemBackground());
    }

    @Test
    public void isDialogMessageAvatarEnabled() {
        assertEquals(true,dialogListStyle.isDialogMessageAvatarEnabled());
    }

    @Test
    public void getDialogMessageAvatarWidth() {
        assertEquals(72,dialogListStyle.getDialogMessageAvatarWidth());
    }

    @Test
    public void getDialogMessageAvatarHeight() {
        assertEquals(72,dialogListStyle.getDialogMessageAvatarHeight());
    }
}