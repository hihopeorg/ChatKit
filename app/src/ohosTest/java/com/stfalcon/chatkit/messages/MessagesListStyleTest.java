package com.stfalcon.chatkit.messages;

import ohos.agp.components.Attr;
import ohos.agp.components.AttrSet;
import ohos.agp.components.element.Element;
import org.junit.Test;

import java.util.Optional;

import static com.stfalcon.chatkit.dialogs.DialogListStyleTest.context;
import static org.junit.Assert.*;

public class MessagesListStyleTest {
    private MessagesListStyle messagesListStyle = MessagesListStyle.parse(context, new AttrSet() {
        @Override
        public Optional<String> getStyle() {
            return Optional.empty();
        }

        @Override
        public int getLength() {
            return 0;
        }

        @Override
        public Optional<Attr> getAttr(int i) {
            return Optional.empty();
        }

        @Override
        public Optional<Attr> getAttr(String s) {
            return Optional.empty();
        }
    });
    @Test
    public void getTextAutoLinkMask() {
        assertEquals(0,messagesListStyle.getTextAutoLinkMask());
    }

    @Test
    public void getIncomingTextLinkColor() {
        assertEquals(-16098825,messagesListStyle.getIncomingTextLinkColor());
    }

    @Test
    public void getOutcomingTextLinkColor() {
        assertEquals(-16098825,messagesListStyle.getOutcomingTextLinkColor());
    }

    @Test
    public void getIncomingAvatarWidth() {
        assertEquals(120,messagesListStyle.getIncomingAvatarWidth());
    }

    @Test
    public void getIncomingAvatarHeight() {
        assertEquals(120,messagesListStyle.getIncomingAvatarHeight());
    }

    @Test
    public void getIncomingDefaultBubblePaddingLeft() {
        assertEquals(48,messagesListStyle.getIncomingDefaultBubblePaddingLeft());
    }

    @Test
    public void getIncomingDefaultBubblePaddingRight() {
        assertEquals(48,messagesListStyle.getIncomingDefaultBubblePaddingRight());
    }

    @Test
    public void getIncomingDefaultBubblePaddingTop() {
        assertEquals(48,messagesListStyle.getIncomingDefaultBubblePaddingTop());
    }

    @Test
    public void getIncomingDefaultBubblePaddingBottom() {
        assertEquals(48,messagesListStyle.getIncomingDefaultBubblePaddingBottom());
    }

    @Test
    public void getIncomingTextColor() {
        assertEquals(-15132133,messagesListStyle.getIncomingTextColor());
    }

    @Test
    public void getIncomingTextSize() {
        assertEquals(51,messagesListStyle.getIncomingTextSize());
    }

    @Test
    public void getIncomingTextStyle() {
        assertEquals(0,messagesListStyle.getIncomingTextStyle());
    }

    @Test
    public void getOutcomingBubbleDrawable() {
        assertEquals(true,messagesListStyle.getOutcomingBubbleDrawable() instanceof Element);
    }

    @Test
    public void getOutcomingImageOverlayDrawable() {
        assertEquals(true,messagesListStyle.getOutcomingImageOverlayDrawable() instanceof Element);
    }

    @Test
    public void getOutcomingDefaultBubblePaddingLeft() {
        assertEquals(48,messagesListStyle.getOutcomingDefaultBubblePaddingLeft());
    }

    @Test
    public void getOutcomingDefaultBubblePaddingRight() {
        assertEquals(48,messagesListStyle.getOutcomingDefaultBubblePaddingRight());
    }

    @Test
    public void getOutcomingDefaultBubblePaddingTop() {
        assertEquals(48,messagesListStyle.getOutcomingDefaultBubblePaddingTop());
    }

    @Test
    public void getOutcomingDefaultBubblePaddingBottom() {
        assertEquals(48,messagesListStyle.getOutcomingDefaultBubblePaddingBottom());
    }

    @Test
    public void getOutcomingTextColor() {
        assertEquals(-1,messagesListStyle.getOutcomingTextColor());
    }

    @Test
    public void getOutcomingTextSize() {
        assertEquals(51,messagesListStyle.getOutcomingTextSize());
    }

    @Test
    public void getOutcomingTextStyle() {
        assertEquals(0,messagesListStyle.getOutcomingTextStyle());
    }

    @Test
    public void getOutcomingTimeTextColor() {
        assertEquals(-1593835521,messagesListStyle.getOutcomingTimeTextColor());
    }

    @Test
    public void getOutcomingTimeTextSize() {
        assertEquals(42,messagesListStyle.getOutcomingTimeTextSize());
    }

    @Test
    public void getOutcomingTimeTextStyle() {
        assertEquals(0,messagesListStyle.getOutcomingTimeTextStyle());
    }

    @Test
    public void getOutcomingImageTimeTextColor() {
        assertEquals(-6842473,messagesListStyle.getOutcomingImageTimeTextColor());
    }

    @Test
    public void getOutcomingImageTimeTextSize() {
        assertEquals(42,messagesListStyle.getOutcomingImageTimeTextSize());
    }

    @Test
    public void getOutcomingImageTimeTextStyle() {
        assertEquals(0,messagesListStyle.getOutcomingImageTimeTextStyle());
    }

    @Test
    public void getDateHeaderTextColor() {
        assertEquals(-6513508,messagesListStyle.getDateHeaderTextColor());
    }

    @Test
    public void getDateHeaderTextSize() {
        assertEquals(48,messagesListStyle.getDateHeaderTextSize());
    }

    @Test
    public void getDateHeaderTextStyle() {
        assertEquals(0,messagesListStyle.getDateHeaderTextStyle());
    }

    @Test
    public void getDateHeaderPadding() {
        assertEquals(48,messagesListStyle.getDateHeaderPadding());
    }

    @Test
    public void getDateHeaderFormat() {
        assertEquals(null,messagesListStyle.getDateHeaderFormat());
    }

    @Test
    public void getIncomingTimeTextSize() {
        assertEquals(42,messagesListStyle.getIncomingTimeTextSize());
    }

    @Test
    public void getIncomingTimeTextStyle() {
        assertEquals(0,messagesListStyle.getIncomingTimeTextStyle());
    }

    @Test
    public void getIncomingTimeTextColor() {
        assertEquals(-6842473,messagesListStyle.getIncomingTimeTextColor());
    }

    @Test
    public void getIncomingImageTimeTextColor() {
        assertEquals(-6842473,messagesListStyle.getIncomingImageTimeTextColor());
    }

    @Test
    public void getIncomingImageTimeTextSize() {
        assertEquals(42,messagesListStyle.getIncomingImageTimeTextSize());
    }

    @Test
    public void getIncomingImageTimeTextStyle() {
        assertEquals(0,messagesListStyle.getIncomingImageTimeTextStyle());
    }

    @Test
    public void getIncomingBubbleDrawable() {
        assertEquals(true,messagesListStyle.getIncomingBubbleDrawable() instanceof Element);
    }

    @Test
    public void getIncomingImageOverlayDrawable() {
        assertEquals(true,messagesListStyle.getIncomingImageOverlayDrawable() instanceof Element);
    }
}