package com.stfalcon.chatkit.sample.features.demo.def;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

import com.stfalcon.chatkit.messages.MessageInput;
import com.stfalcon.chatkit.messages.MessagesList;
import com.stfalcon.chatkit.messages.MessagesListAdapter;
import com.stfalcon.chatkit.sample.ResourceTable;
import com.stfalcon.chatkit.sample.common.data.fixtures.MessagesFixtures;
import com.stfalcon.chatkit.sample.features.demo.DemoMessagesAbility;
import com.stfalcon.chatkit.sample.utils.AppUtils;

public class DefaultMessagesAbility extends DemoMessagesAbility
        implements MessageInput.InputListener,
        MessageInput.AttachmentsListener,
        MessageInput.TypingListener {

    private static final HiLogLabel LABEL = new HiLogLabel(HiLog.LOG_APP, 0x00101, "Typing listener");

    public static void open(Ability context) {
        Intent intent = new Intent();
        Operation operation = new Intent.OperationBuilder()
                .withBundleName(context.getAbilityPackageContext().getBundleName())
                .withAbilityName(DefaultMessagesAbility.class.getCanonicalName())
                .build();
        intent.setOperation(operation);
        context.startAbility(intent);
    }

    private MessagesList messagesList;

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        super.setContentView(ResourceTable.Layout_ability_default_messages);

        this.messagesList = (MessagesList) findComponentById(ResourceTable.Id_messagesList);
        initAdapter();

        MessageInput input = (MessageInput) findComponentById(ResourceTable.Id_input);
        input.setInputListener(this);
        input.setTypingListener(this);
        input.setAttachmentsListener(this);

    }

    @Override
    public boolean onSubmit(CharSequence input) {
        super.messagesAdapter.addToStart(
                MessagesFixtures.getTextMessage(input.toString()), true);
        return true;
    }

    @Override
    public void onAddAttachments() {
        super.messagesAdapter.addToStart(
                MessagesFixtures.getImageMessage(), true);
    }

    private void initAdapter() {
        super.messagesAdapter = new MessagesListAdapter<>(super.senderId, super.imageLoader);
        super.messagesAdapter.enableSelectionMode(this);
        super.messagesAdapter.setLoadMoreListener(this);
        super.messagesAdapter.registerViewClickListener(ResourceTable.Id_messageUserAvatar,
                (view, message) -> AppUtils.showToast(DefaultMessagesAbility.this,
                        message.getUser().getName() + " avatar click",
                        false));
        this.messagesList.setAdapter(super.messagesAdapter);
    }

    @Override
    public void onStartTyping() {
        HiLog.debug(LABEL, getString(ResourceTable.String_start_typing_status));
    }

    @Override
    public void onStopTyping() {
        HiLog.debug(LABEL, getString(ResourceTable.String_stop_typing_status));
    }
}
