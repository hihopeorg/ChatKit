/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licens    es/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.stfalcon.chatkit.sample.utils;

import ohos.agp.components.Image;
import ohos.global.resource.RawFileEntry;
import ohos.media.image.ImageSource;
import ohos.media.image.PixelMap;

import com.stfalcon.chatkit.commons.ImageLoader;

import java.io.IOException;
import java.util.concurrent.ConcurrentHashMap;

public class RawfileImageLoader implements ImageLoader {
    private static final ConcurrentHashMap<String, PixelMap> CACHE = new ConcurrentHashMap<>();

    @Override
    public void loadImage(Image imageView, String url, Object payload) {
        PixelMap pixelMap = null;
        if ((pixelMap = CACHE.get(url)) == null) {
            String fileName = url.substring(url.lastIndexOf("/") + 1);
            RawFileEntry rawFileEntry = imageView.getContext().getResourceManager().getRawFileEntry(
                    "resources/rawfile/" + fileName.replace(".png", ".jpg"));
            try {
                ImageSource imageSource = ImageSource.create(rawFileEntry.openRawFile(), null);
                pixelMap = imageSource.createPixelmap(new ImageSource.DecodingOptions());
                CACHE.put(url, pixelMap);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        imageView.setPixelMap(pixelMap);
    }
}
