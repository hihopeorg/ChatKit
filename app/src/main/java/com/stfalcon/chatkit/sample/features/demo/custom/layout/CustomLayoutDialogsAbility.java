package com.stfalcon.chatkit.sample.features.demo.custom.layout;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;

import com.stfalcon.chatkit.sample.ResourceTable;
import com.stfalcon.chatkit.sample.common.data.fixtures.DialogsFixtures;
import com.stfalcon.chatkit.sample.common.data.model.Dialog;
import com.stfalcon.chatkit.dialogs.DialogsList;
import com.stfalcon.chatkit.dialogs.DialogsListAdapter;
import com.stfalcon.chatkit.sample.features.demo.DemoDialogsAbility;

public class CustomLayoutDialogsAbility extends DemoDialogsAbility {

    public static void open(Ability context) {
        Intent intent = new Intent();
        Operation operation = new Intent.OperationBuilder()
                .withBundleName(context.getAbilityPackageContext().getBundleName())
                .withAbilityName(CustomLayoutDialogsAbility.class.getCanonicalName())
                .build();
        intent.setOperation(operation);
        context.startAbility(intent);
    }

    private DialogsList dialogsList;

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_custom_layout_dialogs);

        dialogsList = (DialogsList) findComponentById(ResourceTable.Id_dialogsList);
        initAdapter();
    }

    @Override
    public void onDialogClick(Dialog dialog) {
        CustomLayoutMessagesAbility.open(this);
    }

    private void initAdapter() {
        super.dialogsAdapter = new DialogsListAdapter<>(ResourceTable.Layout_item_custom_dialog, super.imageLoader);
        super.dialogsAdapter.setItems(DialogsFixtures.getDialogs());

        super.dialogsAdapter.setOnDialogClickListener(this);
        super.dialogsAdapter.setOnDialogLongClickListener(this);

        dialogsList.setAdapter(super.dialogsAdapter);
    }
}
