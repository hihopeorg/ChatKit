package com.stfalcon.chatkit.sample.utils;

import ohos.agp.window.dialog.ToastDialog;
import ohos.app.Context;

/*
 * Created by troy379 on 04.04.17.
 */
public class AppUtils {
    public static final int DURATION_LONG = 3500;
    public static final int DURATION_SHORT = 2000;

    public static void showToast(Context context, int text, boolean isLong) {
        showToast(context, context.getString(text), isLong);
    }

    public static void showToast(Context context, String text, boolean isLong) {
        new ToastDialog(context).setText(text).setDuration(isLong ? DURATION_LONG : DURATION_SHORT).show();
    }
}