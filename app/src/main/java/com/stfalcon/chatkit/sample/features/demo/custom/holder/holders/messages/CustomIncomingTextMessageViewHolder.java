package com.stfalcon.chatkit.sample.features.demo.custom.holder.holders.messages;

import ohos.agp.components.Component;
import ohos.agp.components.element.ElementScatter;

import com.stfalcon.chatkit.sample.ResourceTable;
import com.stfalcon.chatkit.sample.common.data.model.Message;
import com.stfalcon.chatkit.messages.MessageHolders;

public class CustomIncomingTextMessageViewHolder
        extends MessageHolders.IncomingTextMessageViewHolder<Message> {

    private Component onlineIndicator;

    public CustomIncomingTextMessageViewHolder(Component itemView, Object payload) {
        super(itemView, payload);
        onlineIndicator = itemView.findComponentById(ResourceTable.Id_onlineIndicator);
    }

    @Override
    public void onBind(Message message) {
        super.onBind(message);

        boolean isOnline = message.getUser().isOnline();
        if (isOnline) {
            onlineIndicator.setBackground(ElementScatter.getInstance(onlineIndicator.getContext()).parse(ResourceTable.Graphic_shape_bubble_online));
        } else {
            onlineIndicator.setBackground(ElementScatter.getInstance(onlineIndicator.getContext()).parse(ResourceTable.Graphic_shape_bubble_offline));
        }

        //We can set click listener on view from payload
        final Payload payload = (Payload) this.payload;
        userAvatar.setClickedListener(view -> {
            if (payload != null && payload.avatarClickListener != null) {
                payload.avatarClickListener.onAvatarClick();
            }
        });
    }

    public static class Payload {
        public OnAvatarClickListener avatarClickListener;
    }

    public interface OnAvatarClickListener {
        void onAvatarClick();
    }
}
