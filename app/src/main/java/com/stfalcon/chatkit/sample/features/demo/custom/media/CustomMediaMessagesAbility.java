package com.stfalcon.chatkit.sample.features.demo.custom.media;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.agp.components.Button;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.DirectionalLayout;
import ohos.agp.components.Text;
import ohos.agp.window.dialog.CommonDialog;

import com.stfalcon.chatkit.messages.MessageHolders;
import com.stfalcon.chatkit.messages.MessageInput;
import com.stfalcon.chatkit.messages.MessagesList;
import com.stfalcon.chatkit.messages.MessagesListAdapter;
import com.stfalcon.chatkit.sample.ResourceTable;
import com.stfalcon.chatkit.sample.common.data.fixtures.MessagesFixtures;
import com.stfalcon.chatkit.sample.common.data.model.Message;
import com.stfalcon.chatkit.sample.features.demo.DemoMessagesAbility;
import com.stfalcon.chatkit.sample.features.demo.custom.media.holders.IncomingVoiceMessageViewHolder;
import com.stfalcon.chatkit.sample.features.demo.custom.media.holders.OutcomingVoiceMessageViewHolder;

public class CustomMediaMessagesAbility extends DemoMessagesAbility
        implements MessageInput.InputListener,
        MessageInput.AttachmentsListener,
        MessageHolders.ContentChecker<Message>,
        Component.ClickedListener {

    private static final byte CONTENT_TYPE_VOICE = 1;

    public static void open(Ability context) {
        Intent intent = new Intent();
        Operation operation = new Intent.OperationBuilder()
                .withBundleName(context.getAbilityPackageContext().getBundleName())
                .withAbilityName(CustomMediaMessagesAbility.class.getCanonicalName())
                .build();
        intent.setOperation(operation);
        context.startAbility(intent);
    }

    private MessagesList messagesList;

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        super.setContentView(ResourceTable.Layout_ability_custom_media_messages);

        this.messagesList = (MessagesList) findComponentById(ResourceTable.Id_messagesList);
        initAdapter();

        MessageInput input = (MessageInput) findComponentById(ResourceTable.Id_input);
        input.setInputListener(this);
        input.setAttachmentsListener(this);
    }

    @Override
    public boolean onSubmit(CharSequence input) {
        super.messagesAdapter.addToStart(
                MessagesFixtures.getTextMessage(input.toString()), true);
        return true;
    }

    @Override
    public void onAddAttachments() {
        CommonDialog dialog = new CommonDialog(getContext());
        DirectionalLayout layout = new DirectionalLayout(getContext());
        layout.setOrientation(Component.VERTICAL);
        String[] array = getStringArray(ResourceTable.Strarray_view_types_dialog);
        for (int i = 0; i < array.length; i++) {
            Button btn = new Button(getContext());
            btn.setMinHeight(120);
            btn.setId(i);
            btn.setText(array[i]);
            btn.setTextSize(16, Text.TextSizeType.FP);
            btn.setTag(dialog);
            layout.addComponent(btn,
                    new ComponentContainer.LayoutConfig(ComponentContainer.LayoutConfig.MATCH_CONTENT,
                            ComponentContainer.LayoutConfig.MATCH_CONTENT));
            btn.setClickedListener(this);
        }
        dialog.setContentCustomComponent(layout);
        dialog.show();
    }

    @Override
    public boolean hasContentFor(Message message, byte type) {
        if (type == CONTENT_TYPE_VOICE) {
            return message.getVoice() != null
                    && message.getVoice().getUrl() != null
                    && !message.getVoice().getUrl().isEmpty();
        }
        return false;
    }

//    @Override
//    public void onClick(IDialog iDialog, int i) {
//        switch (i) {
//            case 0:
//                messagesAdapter.addToStart(MessagesFixtures.getImageMessage(), true);
//                break;
//            case 1:
//                messagesAdapter.addToStart(MessagesFixtures.getVoiceMessage(), true);
//                break;
//        }
//        iDialog.hide();
//    }

    @Override
    public void onClick(Component component) {
        switch (component.getId()) {
            case 0:
                messagesAdapter.addToStart(MessagesFixtures.getImageMessage(), true);
                break;
            case 1:
                messagesAdapter.addToStart(MessagesFixtures.getVoiceMessage(), true);
                break;
        }
        CommonDialog dialog = (CommonDialog)component.getTag();
        dialog.hide();
    }

    private void initAdapter() {
        MessageHolders holders = new MessageHolders()
                .registerContentType(
                        CONTENT_TYPE_VOICE,
                        IncomingVoiceMessageViewHolder.class,
                        ResourceTable.Layout_item_custom_incoming_voice_message,
                        OutcomingVoiceMessageViewHolder.class,
                        ResourceTable.Layout_item_custom_outcoming_voice_message,
                        this);


        super.messagesAdapter = new MessagesListAdapter<>(super.senderId, holders, super.imageLoader);
        super.messagesAdapter.enableSelectionMode(this);
        super.messagesAdapter.setLoadMoreListener(this);
        this.messagesList.setAdapter(super.messagesAdapter);
    }
}
