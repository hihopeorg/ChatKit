package com.stfalcon.chatkit.sample.features.demo.custom.layout;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;

import com.stfalcon.chatkit.messages.MessageHolders;
import com.stfalcon.chatkit.messages.MessageInput;
import com.stfalcon.chatkit.messages.MessagesList;
import com.stfalcon.chatkit.messages.MessagesListAdapter;
import com.stfalcon.chatkit.sample.ResourceTable;
import com.stfalcon.chatkit.sample.common.data.fixtures.MessagesFixtures;
import com.stfalcon.chatkit.sample.common.data.model.Message;
import com.stfalcon.chatkit.sample.features.demo.DemoMessagesAbility;
import com.stfalcon.chatkit.sample.utils.AppUtils;

public class CustomLayoutMessagesAbility extends DemoMessagesAbility
        implements MessagesListAdapter.OnMessageLongClickListener<Message>,
        MessageInput.InputListener,
        MessageInput.AttachmentsListener {

    public static void open(Ability context) {
        Intent intent = new Intent();
        Operation operation = new Intent.OperationBuilder()
                .withBundleName(context.getAbilityPackageContext().getBundleName())
                .withAbilityName(CustomLayoutMessagesAbility.class.getCanonicalName())
                .build();
        intent.setOperation(operation);
        context.startAbility(intent);
    }

    private MessagesList messagesList;

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        super.setContentView(ResourceTable.Layout_ability_custom_layout_messages);

        messagesList = (MessagesList) findComponentById(ResourceTable.Id_messagesList);
        initAdapter();

        MessageInput input = (MessageInput) findComponentById(ResourceTable.Id_input);
        input.setInputListener(this);
        input.setAttachmentsListener(this);
    }

    @Override
    public boolean onSubmit(CharSequence input) {
        messagesAdapter.addToStart(
                MessagesFixtures.getTextMessage(input.toString()), true);
        return true;
    }

    @Override
    public void onAddAttachments() {
        messagesAdapter.addToStart(MessagesFixtures.getImageMessage(), true);
    }

    @Override
    public void onMessageLongClick(Message message) {
        AppUtils.showToast(this, ResourceTable.String_on_log_click_message, false);
    }

    private void initAdapter() {
        MessageHolders holdersConfig = new MessageHolders()
                .setIncomingTextLayout(ResourceTable.Layout_item_custom_incoming_text_message)
                .setOutcomingTextLayout(ResourceTable.Layout_item_custom_outcoming_text_message)
                .setIncomingImageLayout(ResourceTable.Layout_item_custom_incoming_image_message)
                .setOutcomingImageLayout(ResourceTable.Layout_item_custom_outcoming_image_message);

        super.messagesAdapter = new MessagesListAdapter<>(super.senderId, holdersConfig, super.imageLoader);
        super.messagesAdapter.setOnMessageLongClickListener(this);
        super.messagesAdapter.setLoadMoreListener(this);
        messagesList.setAdapter(super.messagesAdapter);
    }
}
