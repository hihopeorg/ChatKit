package com.stfalcon.chatkit.sample.features.demo;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.Image;
import ohos.agp.components.LayoutScatter;
import ohos.agp.window.service.Window;
import ohos.agp.window.service.WindowManager;
import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

import com.stfalcon.chatkit.commons.ImageLoader;
import com.stfalcon.chatkit.messages.MessagesListAdapter;
import com.stfalcon.chatkit.sample.ResourceTable;
import com.stfalcon.chatkit.sample.common.data.fixtures.MessagesFixtures;
import com.stfalcon.chatkit.sample.common.data.model.Message;
import com.stfalcon.chatkit.sample.utils.AppUtils;
import com.stfalcon.chatkit.sample.utils.RawfileImageLoader;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

/*
 * Created by troy379 on 04.04.17.
 */
public abstract class DemoMessagesAbility extends Ability
        implements MessagesListAdapter.SelectionListener,
        MessagesListAdapter.OnLoadMoreListener {
    private static final HiLogLabel LABEL = new HiLogLabel(HiLog.LOG_APP, 0x00101, "TAG");

    private static final int TOTAL_MESSAGES_COUNT = 100;

    protected final String senderId = "0";
    protected ImageLoader imageLoader;
    protected MessagesListAdapter<Message> messagesAdapter;

    //private Menu menu;
    private int selectionCount;
    private Date lastLoadedDate;

    private Image actionDelete;
    private Image actionCopy;

    private Component.ClickedListener clickedListener = new Component.ClickedListener() {

        @Override
        public void onClick(Component component) {
            switch (component.getId()) {
                case ResourceTable.Id_action_delete:
                    messagesAdapter.deleteSelectedMessages();
                    break;
                case ResourceTable.Id_action_copy:
                    messagesAdapter.copySelectedMessagesText(DemoMessagesAbility.this, getMessageStringFormatter(), true);
                    AppUtils.showToast(DemoMessagesAbility.this, ResourceTable.String_copied_message, true);
                    break;
            }
        }
    };

    public void setContentView(int resId) {
        ComponentContainer container = (ComponentContainer) findComponentById(ResourceTable.Id_container);
        if (container != null) {
            LayoutScatter.getInstance(this).parse(resId, container, true);
        } else {
            setUIContent(resId);
        }
    }

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        Window window = getWindow();
        window.setInputPanelDisplayType(WindowManager.LayoutConfig.INPUT_ADJUST_PAN);
        imageLoader = new RawfileImageLoader();
        setUIContent(ResourceTable.Layout_ability_demo_messages);
        // in case subclass needs to override layout, delay init
        getUITaskDispatcher().asyncDispatch(this::initAbility);
    }

    private void initAbility() {
        actionDelete = (Image) findComponentById(ResourceTable.Id_action_delete);
        actionCopy = (Image) findComponentById(ResourceTable.Id_action_copy);
        if (actionDelete != null) {
            actionDelete.setClickedListener(clickedListener);
        }
        if (actionCopy != null) {
            actionCopy.setClickedListener(clickedListener);
        }
        onSelectionChanged(0);
    }

    @Override
    protected void onActive() {
        super.onActive();
        messagesAdapter.addToStart(MessagesFixtures.getTextMessage(), true);
    }

    @Override
    public void onBackPressed() {
        if (selectionCount == 0) {
            super.onBackPressed();
        } else {
            messagesAdapter.unselectAllItems();
        }
    }

    @Override
    public void onLoadMore(int page, int totalItemsCount) {
        HiLog.info(LABEL, "onLoadMore: " + page + " " + totalItemsCount);
        if (totalItemsCount < TOTAL_MESSAGES_COUNT) {
            loadMessages();
        }
    }

    @Override
    public void onSelectionChanged(int count) {
        this.selectionCount = count;
        if (actionDelete != null) {
            actionDelete.setVisibility(count > 0 ? Component.VISIBLE: Component.INVISIBLE);
        }
        if (actionCopy != null) {
            actionCopy.setVisibility(count > 0 ? Component.VISIBLE: Component.INVISIBLE);
        }
    }

    protected void loadMessages() {
        //imitation of internet connection
        new EventHandler(EventRunner.current()).postTask(() -> {
            ArrayList<Message> messages = MessagesFixtures.getMessages(lastLoadedDate);
            lastLoadedDate = messages.get(messages.size() - 1).getCreatedAt();
            messagesAdapter.addToEnd(messages, false);
        }, 1000);
    }

    private MessagesListAdapter.Formatter<Message> getMessageStringFormatter() {
        return message -> {
            String createdAt = new SimpleDateFormat("MMM d, EEE 'at' h:mm a", Locale.getDefault())
                    .format(message.getCreatedAt());

            String text = message.getText();
            if (text == null) {
                text = "[attachment]";
            }

            return String.format(Locale.getDefault(), "%s: %s (%s)",
                    message.getUser().getName(), text, createdAt);
        };
    }
}
