/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.stfalcon.chatkit.sample.features.demo.component;

import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.Image;
import ohos.agp.components.element.Element;
import ohos.agp.render.PixelMapHolder;
import ohos.app.Context;
import ohos.media.image.PixelMap;

public class CircleImageView extends Image {
    public CircleImageView(Context context) {
        super(context);
        init();
    }

    public CircleImageView(Context context, AttrSet attrSet) {
        super(context, attrSet);
        init();
    }

    public CircleImageView(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
        init();
    }

    private void init() {
        int width = getWidth();
        int height = getHeight();
        if (width > 0 && height > 0) {
            setCornerRadii(new float[]{
                    width / 2, height / 2, width / 2, height / 2, width / 2, height / 2, width / 2, height / 2});
        }
        setLayoutRefreshedListener(new LayoutRefreshedListener() {
            @Override
            public void onRefreshed(Component component) {
                int width = getWidth();
                int height = getHeight();
                setCornerRadii(new float[]{
                        width / 2,
                        height / 2, width / 2, height / 2, width / 2, height / 2, width / 2, height / 2});
                setLayoutRefreshedListener(null);
            }
        });

    }

    @Override
    public void setPixelMap(int resId) {
        super.setPixelMap(resId);
    }

    @Override
    public void setPixelMap(PixelMap pixelMap) {
        super.setPixelMap(pixelMap);
    }

    @Override
    public void setPixelMapHolder(PixelMapHolder pixelMapHolder) {
        super.setPixelMapHolder(pixelMapHolder);
    }

    @Override
    public void setImageElement(Element element) {
        super.setImageElement(element);
    }
}
