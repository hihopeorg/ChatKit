/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.stfalcon.chatkit.sample.utils;

import ohos.agp.components.Component;
import ohos.agp.components.element.PixelMapElement;
import ohos.agp.render.BlendMode;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.render.PixelMapHolder;
import ohos.agp.render.Texture;
import ohos.agp.utils.RectFloat;
import ohos.app.Context;
import ohos.global.resource.NotExistException;
import ohos.media.image.ImageSource;
import ohos.media.image.PixelMap;
import ohos.media.image.common.PixelFormat;
import ohos.media.image.common.Rect;
import ohos.media.image.common.Size;
import ohos.utils.PlainArray;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class NinePatchHelper {
    public static final int TRANSPARENT = 0x00000000;
    public static final int OPAQUE = 0xff000000;

    private static final boolean USE_CACHE = true;

    private static final PlainArray<NinePatchInfo> CACHE = new PlainArray<>();

    private static class NinePatchInfo {
        /**
         * a nine patch pic like:
         * (p means visible pixels, 0 means transparent alpha, 1 meas opaque alpha)
         * 0 0 0 1 0 0 0
         * 0 p p p p p 0
         * 1 p p p p p 1
         * 0 p p p p p 1
         * 0 0 1 1 1 0 0
         * will be parsed to:
         * visibleWidth: 5
         * visibleHeight: 3
         * scalablePixelsHorizontal: 1
         * scalablePixelsVertical: 1
         * unscalablePixelsHorizontal: 4
         * unscalablePixelsVertical: 2
         * paddingLeft: 1
         * paddingTop = 1
         * paddingRight: 1
         * paddingBottom: 0
         * horizontalDiffs: [2, 1, 2]
         * verticalDiffs: [1, 1, 1]
         */
        int visibleWidth;
        int visibleHeight;
        int scalablePixelsHorizontal;
        int scalablePixelsVertical;
        int unscalablePixelsHorizontal;
        int unscalablePixelsVertical;
        int paddingLeft = -1;
        int paddingTop = -1;
        int paddingRight = -1;
        int paddingBottom = -1;
        // should always with an odd size, the even indices carry unscalable area sizes, the odd indices carry
        // scalable area sizes
        int[] horizontalDiffs;
        int[] verticalDiffs;
        boolean isLegalNinePatch = false;
        PixelMap pixelMap;

        private NinePatchInfo(int resId, Context context) {
            this(decodePixelMap(resId, context), true);
        }

        private NinePatchInfo(PixelMap pixelMap) {
            this(pixelMap, false);
        }

        private NinePatchInfo(PixelMap pixelMap, boolean fromId) {
            if (pixelMap == null || pixelMap.isReleased()) {
                return;
            }

            // check size
            int width = pixelMap.getImageInfo().size.width;
            int height = pixelMap.getImageInfo().size.height;
            if (width < 2 || height < 2) {
                return;
            }
            visibleWidth = width - 2;
            visibleHeight = height - 2;

            // check bounds and corners
            int[] leftLine = new int[height];
            int[] topLine = new int[width];
            int[] rightLine = new int[height];
            int[] bottomLine = new int[width];
            pixelMap.readPixels(leftLine, 0, 1, new Rect(0, 0, 1, height));
            pixelMap.readPixels(topLine, 0, width, new Rect(0, 0, width, 1));
            pixelMap.readPixels(rightLine, 0, 1, new Rect(width - 1, 0, 1, height));
            pixelMap.readPixels(bottomLine, 0, width, new Rect(0, height - 1, width, 1));
            if (Arrays.stream(leftLine).filter(i -> i != TRANSPARENT && i != OPAQUE).count() > 0 ||
                    Arrays.stream(topLine).filter(i -> i != TRANSPARENT && i != OPAQUE).count() > 0 ||
                    Arrays.stream(rightLine).filter(i -> i != TRANSPARENT && i != OPAQUE).count() > 0 ||
                    Arrays.stream(bottomLine).filter(i -> i != TRANSPARENT && i != OPAQUE).count() > 0) {
                return;
            }
            if (topLine[0] != TRANSPARENT || topLine[width - 1] != TRANSPARENT || bottomLine[0] != TRANSPARENT
                    || bottomLine[width - 1] != TRANSPARENT) {
                return;
            }

            // parse data
            List<Integer> horizontalIntervals = new ArrayList<>(3);
            List<Integer> verticalIntervals = new ArrayList<>(3);
            int lastPixel = TRANSPARENT;
            int lastBorder = 1;
            int currentPixel;

            for (int i = 1; i < leftLine.length - 1; i++) {
                currentPixel = leftLine[i];
                if (currentPixel != lastPixel) {
                    verticalIntervals.add(i - lastBorder);
                    lastBorder = i;
                }
                lastPixel = currentPixel;
            }
            verticalIntervals.add(height - lastBorder - 1);
            if ((verticalIntervals.size() & 1) == 0) {
                verticalIntervals.add(0);
            }

            lastPixel = TRANSPARENT;
            lastBorder = 1;
            for (int i = 1; i < topLine.length - 1; i++) {
                currentPixel = topLine[i];
                if (currentPixel != lastPixel) {
                    horizontalIntervals.add(i - lastBorder);
                    lastBorder = i;
                }
                lastPixel = currentPixel;
            }
            horizontalIntervals.add(width - lastBorder - 1);
            if ((horizontalIntervals.size() & 1) == 0) {
                verticalIntervals.add(0);
            }

            lastPixel = TRANSPARENT;
            for (int i = 1; i < rightLine.length; i++) {
                currentPixel = rightLine[i];
                if (currentPixel == OPAQUE && lastPixel == TRANSPARENT && paddingTop == -1) {
                    paddingTop = i - 1;
                }
                if (currentPixel == TRANSPARENT && lastPixel == OPAQUE) {
                    paddingBottom = height - i - 1;
                }
                lastPixel = currentPixel;
            }
            if (paddingTop == -1) {// no black point in right line, use scaled range as content range
                paddingTop = verticalIntervals.get(0);
                paddingBottom = verticalIntervals.get(verticalIntervals.size() - 1);
            }

            lastPixel = TRANSPARENT;
            for (int i = 1; i < bottomLine.length; i++) {
                currentPixel = bottomLine[i];
                if (currentPixel == OPAQUE && lastPixel == TRANSPARENT && paddingLeft == -1) {
                    paddingLeft = i - 1;
                }
                if (currentPixel == TRANSPARENT && lastPixel == OPAQUE) {
                    paddingRight = width - i - 1;
                }
                lastPixel = currentPixel;
            }
            if (paddingLeft == -1) {// no black point in bottom line, use scaled range as content range
                paddingLeft = horizontalIntervals.get(0);
                paddingRight = horizontalIntervals.get(horizontalIntervals.size() - 1);
            }

            horizontalDiffs = new int[horizontalIntervals.size()];
            for (int i = 0; i < horizontalIntervals.size(); i++) {
                horizontalDiffs[i] = horizontalIntervals.get(i);
            }
            verticalDiffs = new int[verticalIntervals.size()];
            for (int i = 0; i < verticalIntervals.size(); i++) {
                verticalDiffs[i] = verticalIntervals.get(i);
            }
            for (int i = 1; i < horizontalIntervals.size(); i += 2) {
                scalablePixelsHorizontal += horizontalIntervals.get(i);
            }
            for (int i = 1; i < verticalIntervals.size(); i += 2) {
                scalablePixelsVertical += verticalIntervals.get(i);
            }
            unscalablePixelsHorizontal = visibleWidth - scalablePixelsHorizontal;
            unscalablePixelsVertical = visibleHeight - scalablePixelsVertical;
            if (fromId && USE_CACHE) {
                this.pixelMap = pixelMap;
            }
            isLegalNinePatch = true;
        }

        @Override
        public String toString() {
            StringBuilder sb = new StringBuilder(super.toString());
            sb.append(", isLegalNinePatch = ").append(isLegalNinePatch)
                    .append(", visibleWidth = ").append(visibleWidth)
                    .append(", visibleHeight = ").append(visibleHeight)
                    .append(", scalablePixelsHorizontal = ").append(scalablePixelsHorizontal)
                    .append(", scalablePixelsVertical = ").append(scalablePixelsVertical)
                    .append(", unscalablePixelsHorizontal = ").append(unscalablePixelsHorizontal)
                    .append(", unscalablePixelsVertical = ").append(unscalablePixelsVertical)
                    .append(", paddingLeft = ").append(paddingLeft)
                    .append(", paddingTop = ").append(paddingTop)
                    .append(", paddingRight = ").append(paddingRight)
                    .append(", paddingBottom = ").append(paddingBottom)
                    .append(", horizontalDiffs = ").append(Arrays.toString(horizontalDiffs))
                    .append(", verticalDiffs = ").append(Arrays.toString(verticalDiffs));
            return sb.toString();
        }
    }

    /**
     * Warning:
     * This method must be called after component's arranging layout or the component has been given a precise size
     * by xml/LayoutConfig,
     * otherwise the component's size is illegal.
     *
     * @param resId
     * @param component
     */
    public static void setNinePatchBackground(int resId, Component component) {
        if (component == null || component.getHeight() <= 0 || component.getWidth() <= 0) {
            return;
        }
        if (USE_CACHE) {
            NinePatchInfo ninePatchInfo = CACHE.get(resId, null);
            if (ninePatchInfo == null) {
                ninePatchInfo = new NinePatchInfo(resId, component.getContext());
                if (!ninePatchInfo.isLegalNinePatch) {
                    return;
                }
                CACHE.put(resId, ninePatchInfo);
            }
            setNinePatchBackground(ninePatchInfo.pixelMap, component, ninePatchInfo);
        } else {
            setNinePatchBackground(decodePixelMap(resId, component.getContext()), component);
        }

    }

    /**
     * Warning:
     * This method must be called after component's arranging layout or the component has been given a precise size
     * by xml/LayoutConfig,
     * otherwise the component's size is illegal.
     *
     * @param pixelMap
     * @param component
     */
    public static void setNinePatchBackground(PixelMap pixelMap, Component component) {
        if (pixelMap == null || pixelMap.isReleased() || component == null || component.getHeight() <= 0
                || component.getWidth() <= 0) {
            return;
        }
        NinePatchInfo ninePatchInfo = new NinePatchInfo(pixelMap);
        if (!ninePatchInfo.isLegalNinePatch) {
            return;
        }
        setNinePatchBackground(pixelMap, component, ninePatchInfo);
    }

    private static void setNinePatchBackground(PixelMap pixelMap, Component component, NinePatchInfo ninePatchInfo) {
        component.setMinWidth(Math.max(component.getMinWidth(), ninePatchInfo.unscalablePixelsHorizontal));
        component.setMinHeight(Math.max(component.getMinHeight(), ninePatchInfo.unscalablePixelsVertical));
        component.setPadding(ninePatchInfo.paddingLeft, ninePatchInfo.paddingTop,
                ninePatchInfo.paddingRight, ninePatchInfo.paddingBottom);
        PixelMap.InitializationOptions initializationOptions = new PixelMap.InitializationOptions();
        initializationOptions.size = new Size(component.getWidth(), component.getHeight());
        initializationOptions.pixelFormat = PixelFormat.ARGB_8888;
        initializationOptions.editable = true;
        PixelMap newPixelMap = PixelMap.create(initializationOptions);
        Canvas canvas = new Canvas(new Texture(newPixelMap));
        drawNinePatchToCanvas(pixelMap, ninePatchInfo, canvas, component.getWidth(), component.getHeight());
        component.setBackground(new PixelMapElement(newPixelMap));
    }

    public static void setNinePatchDrawTask(int resId, Component component) {
        if (component == null) {
            return;
        }
        if (USE_CACHE) {
            NinePatchInfo ninePatchInfo = CACHE.get(resId, null);
            if (ninePatchInfo == null) {
                ninePatchInfo = new NinePatchInfo(resId, component.getContext());
                if (!ninePatchInfo.isLegalNinePatch) {
                    return;
                }
                CACHE.put(resId, ninePatchInfo);
            }
            setNinePatchDrawTask(ninePatchInfo.pixelMap, component, ninePatchInfo);
        } else {
            setNinePatchDrawTask(decodePixelMap(resId, component.getContext()), component);
        }
    }

    public static void setNinePatchDrawTask(PixelMap pixelMap, Component component) {
        if (pixelMap == null || pixelMap.isReleased() || component == null) {
            return;
        }
        NinePatchInfo ninePatchInfo = new NinePatchInfo(pixelMap);
        if (!ninePatchInfo.isLegalNinePatch) {
            return;
        }
        setNinePatchDrawTask(pixelMap, component, ninePatchInfo);
    }

    private static void setNinePatchDrawTask(PixelMap pixelMap, Component component, NinePatchInfo ninePatchInfo) {
        component.setMinWidth(Math.max(component.getMinWidth(), ninePatchInfo.unscalablePixelsHorizontal));
        component.setMinHeight(Math.max(component.getMinHeight(), ninePatchInfo.unscalablePixelsVertical));
        component.setPadding(ninePatchInfo.paddingLeft, ninePatchInfo.paddingTop,
                ninePatchInfo.paddingRight, ninePatchInfo.paddingBottom);
        component.addDrawTask(new Component.DrawTask() {
            @Override
            public void onDraw(Component component, Canvas canvas) {
                drawNinePatchToCanvas(pixelMap, ninePatchInfo, canvas, component.getWidth(), component.getHeight());
            }
        }, Component.DrawTask.BETWEEN_BACKGROUND_AND_CONTENT);
    }

    public static PixelMap getScaledNinePatchPixelMap(int resId, Context context, int dstWidth, int dstHeight) {
        if (USE_CACHE) {
            NinePatchInfo ninePatchInfo = CACHE.get(resId, null);
            if (ninePatchInfo == null) {
                ninePatchInfo = new NinePatchInfo(resId, context);
                if (!ninePatchInfo.isLegalNinePatch) {
                    return null;
                }
                CACHE.put(resId, ninePatchInfo);
            }
            return getScaledNinePatchPixelMap(ninePatchInfo.pixelMap, dstWidth, dstHeight);
        } else {
            return getScaledNinePatchPixelMap(decodePixelMap(resId, context), dstWidth, dstHeight);
        }
    }

    public static PixelMap getScaledNinePatchPixelMap(PixelMap pixelMap, int dstWidth, int dstHeight) {
        if (pixelMap == null || pixelMap.isReleased()) {
            return null;
        }
        NinePatchInfo ninePatchInfo = new NinePatchInfo(pixelMap);
        if (!ninePatchInfo.isLegalNinePatch) {
            return null;
        }
        PixelMap.InitializationOptions initializationOptions = new PixelMap.InitializationOptions();
        initializationOptions.size = new Size(dstWidth, dstHeight);
        initializationOptions.pixelFormat = PixelFormat.ARGB_8888;
        initializationOptions.editable = true;
        PixelMap newPixelMap = PixelMap.create(initializationOptions);
        Canvas canvas = new Canvas(new Texture(newPixelMap));
        drawNinePatchToCanvas(pixelMap, ninePatchInfo, canvas, dstWidth, dstHeight);
        return newPixelMap;
    }

    public static PixelMap getMask(PixelMap pixelMap) {
        int width = pixelMap.getImageInfo().size.width;
        int height = pixelMap.getImageInfo().size.height;
        int[] pixels = new int[width * height];
        pixelMap.readPixels(pixels, 0, width, new Rect(0, 0, width, height));
        for (int i = 0; i < pixels.length; i++) {
            pixels[i] = (pixels[i] == 0 ? -1 : 0);
        }
        pixelMap.writePixels(pixels, 0, width, new Rect(0, 0, width, height));
        return pixelMap;
    }

    private static void drawNinePatchToCanvas(PixelMap pixelMap, NinePatchInfo ninePatchInfo, Canvas canvas,
                                              int dstWidth,
                                              int dstHeight) {
        if (pixelMap.isReleased()) {
            return;
        }
        Paint paint = new Paint();
        paint.setBlendMode(BlendMode.SRC_OVER);

        float scaleFractionHorizontal =
                (dstWidth - ninePatchInfo.unscalablePixelsHorizontal) * 1f / ninePatchInfo.scalablePixelsHorizontal;
        float scaleFractionVertical =
                (dstHeight - ninePatchInfo.unscalablePixelsVertical) * 1f / ninePatchInfo.scalablePixelsVertical;
        PixelMapHolder pixelMapHolder = new PixelMapHolder(pixelMap);
        RectFloat rectSrc = new RectFloat(1, 1, 1, 1);
        RectFloat rectDst = new RectFloat();
        int[] scaledHorizontalDiffs = ninePatchInfo.horizontalDiffs.clone();
        int[] scaledVerticalDiffs = ninePatchInfo.verticalDiffs.clone();
        for (int i = 1; i < scaledHorizontalDiffs.length; i += 2) {
            scaledHorizontalDiffs[i] = (int) (scaledHorizontalDiffs[i] * scaleFractionHorizontal + 0.5f);
        }
        for (int i = 1; i < scaledVerticalDiffs.length; i += 2) {
            scaledVerticalDiffs[i] = (int) (scaledVerticalDiffs[i] * scaleFractionVertical + 0.5f);
        }
        for (int i = 0; i < scaledVerticalDiffs.length; i++) {
            rectSrc.modify(1, rectSrc.bottom, 1, rectSrc.bottom + ninePatchInfo.verticalDiffs[i]);
            rectDst.modify(0, rectDst.bottom, 0, rectDst.bottom + scaledVerticalDiffs[i]);
            for (int j = 0; j < scaledHorizontalDiffs.length; j++) {
                rectSrc.modify(rectSrc.right, rectSrc.top,
                        rectSrc.right + ninePatchInfo.horizontalDiffs[j], rectSrc.bottom);
                rectDst.modify(rectDst.right, rectDst.top, rectDst.right + scaledHorizontalDiffs[j], rectDst.bottom);
                canvas.drawPixelMapHolderRect(pixelMapHolder, rectSrc, rectDst, paint);
            }
        }
    }

    public static PixelMap decodePixelMap(int resId, Context context) {
        if (context == null) {
            return null;
        }
        PixelMap pixelMap = null;
        try {
            ImageSource imageSource = ImageSource.create(context.getResourceManager().getResource(resId), null);
            pixelMap = imageSource.createPixelmap(new ImageSource.DecodingOptions());
        } catch (IOException e) {
            e.printStackTrace();
        } catch (NotExistException e) {
            e.printStackTrace();
        }
        return pixelMap;
    }
}
