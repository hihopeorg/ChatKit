package com.stfalcon.chatkit.sample.features.main.adapter;

import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.PageSliderProvider;
import ohos.app.Context;

import com.stfalcon.chatkit.sample.ResourceTable;

/*
 * Created by troy379 on 11.04.17.
 */
public class MainActivityPagerAdapter extends PageSliderProvider {

    public static final int ID_DEFAULT = 0;
    public static final int ID_STYLED = 1;
    public static final int ID_CUSTOM_LAYOUT = 2;
    public static final int ID_CUSTOM_VIEW_HOLDER = 3;
    public static final int ID_CUSTOM_CONTENT = 4;

    private final Context context;
    private int pageMargin;

    public MainActivityPagerAdapter(Context context) {
        super();
        this.context = context;
    }

    public void setPageMargin(int pageMargin) {
        this.pageMargin = pageMargin;
    }

    @Override
    public Object createPageInContainer(ComponentContainer componentContainer, int position) {
        String title = null;
        String description = null;
        switch (position) {
            case ID_DEFAULT:
                title = context.getString(ResourceTable.String_sample_title_default);
                description = context.getString(ResourceTable.String_sample_subtitle_default);
                break;
            case ID_STYLED:
                title = context.getString(ResourceTable.String_sample_title_attrs);
                description = context.getString(ResourceTable.String_sample_subtitle_attrs);
                break;
            case ID_CUSTOM_LAYOUT:
                title = context.getString(ResourceTable.String_sample_title_layout);
                description = context.getString(ResourceTable.String_sample_subtitle_layout);
                break;
            case ID_CUSTOM_VIEW_HOLDER:
                title = context.getString(ResourceTable.String_sample_title_holder);
                description = context.getString(ResourceTable.String_sample_subtitle_holder);
                break;
            case ID_CUSTOM_CONTENT:
                title = context.getString(ResourceTable.String_sample_title_custom_content);
                description = context.getString(ResourceTable.String_sample_subtitle_custom_content);
                break;
        }
        DemoCardLayout page = DemoCardLayout.newInstance(context, position, title, description);
        ComponentContainer.LayoutConfig config =
                new ComponentContainer.LayoutConfig(ComponentContainer.LayoutConfig.MATCH_PARENT,
                ComponentContainer.LayoutConfig.MATCH_PARENT);

        config.setMargins(pageMargin,pageMargin,pageMargin,pageMargin);
        componentContainer.addComponent(page, config);
        return page;
    }

    @Override
    public void destroyPageFromContainer(ComponentContainer componentContainer, int i, Object o) {
        DemoCardLayout page = (DemoCardLayout)o;
        componentContainer.removeComponent(page);
    }

    @Override
    public boolean isPageMatchToObject(Component component, Object o) {
        return true;
    }

    @Override
    public int getCount() {
        return 5;
    }
}
