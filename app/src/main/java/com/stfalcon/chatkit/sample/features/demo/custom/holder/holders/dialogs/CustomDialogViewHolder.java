package com.stfalcon.chatkit.sample.features.demo.custom.holder.holders.dialogs;

import ohos.agp.components.Component;
import ohos.agp.components.element.ElementScatter;

import com.stfalcon.chatkit.sample.ResourceTable;
import com.stfalcon.chatkit.sample.common.data.model.Dialog;
import com.stfalcon.chatkit.dialogs.DialogsListAdapter;

/*
 * Created by Anton Bevza on 1/18/17.
 */
public class CustomDialogViewHolder
        extends DialogsListAdapter.DialogViewHolder<Dialog> {

    private Component onlineIndicator;

    public CustomDialogViewHolder(Component itemView) {
        super(itemView);
        onlineIndicator = itemView.findComponentById(ResourceTable.Id_onlineIndicator);
    }

    @Override
    public void onBind(Dialog dialog) {
        super.onBind(dialog);

        if (dialog.getUsers().size() > 1) {
            onlineIndicator.setVisibility(Component.HIDE);
        } else {
            boolean isOnline = dialog.getUsers().get(0).isOnline();
            onlineIndicator.setVisibility(Component.VISIBLE);
            if (isOnline) {
                onlineIndicator.setBackground(ElementScatter.getInstance(onlineIndicator.getContext()).parse(ResourceTable.Graphic_shape_bubble_online));
            } else {
                onlineIndicator.setBackground(ElementScatter.getInstance(onlineIndicator.getContext()).parse(ResourceTable.Graphic_shape_bubble_offline));
            }
        }
    }
}
