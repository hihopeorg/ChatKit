package com.stfalcon.chatkit.sample.features.demo.custom.media.holders;

import ohos.agp.components.Component;
import ohos.agp.components.Text;

import com.stfalcon.chatkit.sample.ResourceTable;
import com.stfalcon.chatkit.sample.common.data.model.Message;
import com.stfalcon.chatkit.messages.MessageHolders;
import com.stfalcon.chatkit.sample.utils.FormatUtils;
import com.stfalcon.chatkit.utils.DateFormatter;

/*
 * Created by troy379 on 05.04.17.
 */
public class IncomingVoiceMessageViewHolder
        extends MessageHolders.IncomingTextMessageViewHolder<Message> {

    private Text tvDuration;
    private Text tvTime;

    public IncomingVoiceMessageViewHolder(Component itemView, Object payload) {
        super(itemView, payload);
        tvDuration = (Text) itemView.findComponentById(ResourceTable.Id_duration);
        tvTime = (Text) itemView.findComponentById(ResourceTable.Id_time);
    }

    @Override
    public void onBind(Message message) {
        super.onBind(message);
        tvDuration.setText(
                FormatUtils.getDurationString(
                        message.getVoice().getDuration()));
        tvTime.setText(DateFormatter.format(message.getCreatedAt(), DateFormatter.Template.TIME));
    }
}
