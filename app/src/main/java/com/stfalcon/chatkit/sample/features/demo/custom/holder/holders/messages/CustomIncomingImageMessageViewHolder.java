package com.stfalcon.chatkit.sample.features.demo.custom.holder.holders.messages;

import ohos.agp.components.Component;
import ohos.agp.components.element.ElementScatter;

import com.stfalcon.chatkit.sample.ResourceTable;
import com.stfalcon.chatkit.sample.common.data.model.Message;
import com.stfalcon.chatkit.messages.MessageHolders;

/*
 * Created by troy379 on 05.04.17.
 */
public class CustomIncomingImageMessageViewHolder
        extends MessageHolders.IncomingImageMessageViewHolder<Message> {

    private Component onlineIndicator;

    public CustomIncomingImageMessageViewHolder(Component itemView, Object payload) {
        super(itemView, payload);
        onlineIndicator = itemView.findComponentById(ResourceTable.Id_onlineIndicator);
    }

    @Override
    public void onBind(Message message) {
        super.onBind(message);

        boolean isOnline = message.getUser().isOnline();
        if (isOnline) {
            onlineIndicator.setBackground(ElementScatter.getInstance(onlineIndicator.getContext()).parse(ResourceTable.Graphic_shape_bubble_online));
        } else {
            onlineIndicator.setBackground(ElementScatter.getInstance(onlineIndicator.getContext()).parse(ResourceTable.Graphic_shape_bubble_offline));
        }
    }
}