package com.stfalcon.chatkit.sample.features.main.adapter;

import ohos.agp.components.Button;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentTreeObserver;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.StackLayout;
import ohos.agp.components.Text;
import ohos.app.Context;
import ohos.utils.PacMap;

import com.stfalcon.chatkit.sample.ResourceTable;

/*
 * Created by troy379 on 11.04.17.
 */
public class DemoCardLayout extends StackLayout
        implements Component.ClickedListener, Component.BindStateChangedListener {

    private static final String ARG_ID = "id";
    private static final String ARG_TITLE = "title";
    private static final String ARG_DESCRIPTION = "description";

    private int id;
    private String title, description;
    private OnActionListener actionListener;
    private PacMap args = new PacMap();

    public DemoCardLayout(Context context, PacMap args) {
        super(context);
        this.args.putAll(args);
        init();
    }

    public static DemoCardLayout newInstance(Context context, int id, String title, String description) {
        PacMap args = new PacMap();
        args.putIntValue(ARG_ID, id);
        args.putString(ARG_TITLE, title);
        args.putString(ARG_DESCRIPTION, description);
        DemoCardLayout layout = new DemoCardLayout(context, args);
        return layout;
    }

    public PacMap getArguments() {
        return args;
    }

    private void init() {
        if (getArguments() != null) {
            this.id = getArguments().getIntValue(ARG_ID);
            this.title = getArguments().getString(ARG_TITLE);
            this.description = getArguments().getString(ARG_DESCRIPTION);
        }

        Component v = LayoutScatter.getInstance(getContext()).parse(ResourceTable.Layout_fragment_demo_card, this, true);
        Text tvTitle = (Text) v.findComponentById(ResourceTable.Id_tvTitle);
        Text tvDescription = (Text) v.findComponentById(ResourceTable.Id_tvDescription);
        Button button = (Button) v.findComponentById(ResourceTable.Id_button);

        tvTitle.setText(title);
        tvDescription.setText(description);
        button.setClickedListener(this);
        this.setBindStateChangedListener(this);
    }

    @Override
    public void onClick(Component view) {
        if (view.getId() == ResourceTable.Id_button) {
            onAction();
        }
    }

    public void onAction() {
        if (actionListener != null) {
            actionListener.onAction(this.id);
        }
    }


    @Override
    public void onComponentBoundToWindow(Component component) {
        Context context = getContext();
        if (context instanceof OnActionListener) {
            actionListener = (OnActionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnActionListener");
        }
    }

    @Override
    public void onComponentUnboundFromWindow(Component component) {
        actionListener = null;
    }

    public interface OnActionListener {
        void onAction(int id);
    }
}