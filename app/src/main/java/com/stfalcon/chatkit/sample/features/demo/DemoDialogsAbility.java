package com.stfalcon.chatkit.sample.features.demo;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;

import com.stfalcon.chatkit.commons.ImageLoader;
import com.stfalcon.chatkit.dialogs.DialogsListAdapter;
import com.stfalcon.chatkit.sample.common.data.model.Dialog;
import com.stfalcon.chatkit.sample.utils.AppUtils;
import com.stfalcon.chatkit.sample.utils.RawfileImageLoader;

/*
 * Created by troy379 on 05.04.17.
 */
public abstract class DemoDialogsAbility extends Ability
        implements DialogsListAdapter.OnDialogClickListener<Dialog>,
        DialogsListAdapter.OnDialogLongClickListener<Dialog> {

    protected ImageLoader imageLoader;
    protected DialogsListAdapter<Dialog> dialogsAdapter;

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        imageLoader = new RawfileImageLoader();
    }

    @Override
    public void onDialogLongClick(Dialog dialog) {
        AppUtils.showToast(
                this,
                dialog.getDialogName(),
                false);
    }
}
