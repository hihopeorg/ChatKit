package com.stfalcon.chatkit.sample.features.main;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.components.PageSlider;
import ohos.agp.components.PageSliderIndicator;

import com.stfalcon.chatkit.sample.ResourceTable;
import com.stfalcon.chatkit.sample.features.demo.custom.holder.CustomHolderDialogsAbility;
import com.stfalcon.chatkit.sample.features.demo.custom.layout.CustomLayoutDialogsAbility;
import com.stfalcon.chatkit.sample.features.demo.custom.media.CustomMediaMessagesAbility;
import com.stfalcon.chatkit.sample.features.demo.def.DefaultDialogsAbility;
import com.stfalcon.chatkit.sample.features.demo.styled.StyledDialogsAbility;
import com.stfalcon.chatkit.sample.features.main.adapter.DemoCardLayout;
import com.stfalcon.chatkit.sample.features.main.adapter.MainActivityPagerAdapter;
import com.stfalcon.chatkit.utils.ResourceHelper;

/*
 * Created by troy379 on 04.04.17.
 */
public class MainAbility extends Ability
        implements DemoCardLayout.OnActionListener {

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);

        PageSlider pager = (PageSlider) findComponentById(ResourceTable.Id_pager);
        MainActivityPagerAdapter adapter = new MainActivityPagerAdapter(this);
        adapter.setPageMargin(
                ResourceHelper.getDimensionFromVp(ResourceTable.Float_card_padding_vp, getContext(), 0) / 4);
        pager.setProvider(new MainActivityPagerAdapter(this));

        //pager.setPageCacheSize(3);

        PageSliderIndicator indicator = (PageSliderIndicator) findComponentById(ResourceTable.Id_indicator);
        indicator.setViewPager(pager);

    }

    @Override
    public void onAction(int id) {
        switch (id) {
            case MainActivityPagerAdapter.ID_DEFAULT:
                DefaultDialogsAbility.open(this);
                break;
            case MainActivityPagerAdapter.ID_STYLED:
                StyledDialogsAbility.open(this);
                break;
            case MainActivityPagerAdapter.ID_CUSTOM_LAYOUT:
                CustomLayoutDialogsAbility.open(this);
                break;
            case MainActivityPagerAdapter.ID_CUSTOM_VIEW_HOLDER:
                CustomHolderDialogsAbility.open(this);
                break;
            case MainActivityPagerAdapter.ID_CUSTOM_CONTENT:
                CustomMediaMessagesAbility.open(this);
                break;
        }
    }
}
