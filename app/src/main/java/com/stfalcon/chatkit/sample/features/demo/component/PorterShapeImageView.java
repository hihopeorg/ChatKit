/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.stfalcon.chatkit.sample.features.demo.component;

import ohos.agp.components.AttrHelper;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.Image;
import ohos.agp.components.element.Element;
import ohos.agp.components.element.PixelMapElement;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.render.Path;
import ohos.agp.render.PixelMapHolder;
import ohos.agp.utils.RectFloat;
import ohos.app.Context;
import ohos.media.image.PixelMap;

import com.stfalcon.chatkit.sample.utils.NinePatchHelper;
import com.stfalcon.chatkit.utils.ResourceHelper;

public class PorterShapeImageView extends Image implements Component.EstimateSizeListener, Component.DrawTask {
    private boolean isRight;
    private PixelMap pixelMap;

    public PorterShapeImageView(Context context) {
        super(context);
    }

    public PorterShapeImageView(Context context, AttrSet attrSet) {
        super(context, attrSet);
        init(attrSet);
    }

    public PorterShapeImageView(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
        init(attrSet);
    }

    private void init(AttrSet attrSet) {
        isRight = "right".equals(ResourceHelper.getString(attrSet, "orientation", null));
        setScaleMode(ScaleMode.INSIDE);
        setEstimateSizeListener(this);
        addDrawTask(this);
    }

    @Override
    public void setImageElement(Element element) {
        if (element instanceof PixelMapElement) {
            pixelMap = ((PixelMapElement) element).getPixelMap();
        }
        invalidate();
    }

    @Override
    public void setPixelMap(int resId) {
        pixelMap = NinePatchHelper.decodePixelMap(resId, getContext());
        invalidate();
    }

    @Override
    public void setPixelMap(PixelMap pixelMap) {
        this.pixelMap = pixelMap;
        invalidate();
    }

    @Override
    public void setPixelMapHolder(PixelMapHolder pixelMapHolder) {
        pixelMap = pixelMapHolder.getPixelMap();
        invalidate();
    }

    @Override
    public void onDraw(Component component, Canvas canvas) {
        int width = getWidth();
        int height = getHeight();

        Path clipPath = new Path();
        int baseL = AttrHelper.vp2px(10, getContext());
        // use clippath instead of use image mask
        if (isRight) {
            clipPath.moveTo(baseL, 0);
            clipPath.lineTo(width - 3 * baseL, 0);
            clipPath.arcTo(new RectFloat(width - 3 * baseL, 0, width - baseL, baseL * 2), 270, 90);
            clipPath.lineTo(width - baseL / 5, baseL * 16 / 10);
            clipPath.arcTo(new RectFloat(width - baseL, baseL * 3 / 2, width, baseL * 5 / 2), -53.13f, 106.26f);
            clipPath.lineTo(width - baseL, baseL * 3);
            clipPath.lineTo(width - baseL, height - baseL);
            clipPath.arcTo(new RectFloat(width - 3 * baseL, height - 2 * baseL, width - baseL, height), 0, 90);
            clipPath.lineTo(baseL, height);
            clipPath.arcTo(new RectFloat(0, height - 2 * baseL, baseL * 2, height), 90, 90);
            clipPath.lineTo(0, baseL);
            clipPath.arcTo(new RectFloat(0, 0, baseL * 2, baseL * 2), 180, 90);
        } else {
            clipPath.moveTo(baseL * 2, 0);
            clipPath.lineTo(width - baseL, 0);
            clipPath.arcTo(new RectFloat(width - 2 * baseL, 0, width, baseL * 2), 270, 90);
            clipPath.lineTo(width, height - baseL);
            clipPath.arcTo(new RectFloat(width - 2 * baseL, height - 2 * baseL, width, height), 0, 90);
            clipPath.lineTo(baseL * 2, height);
            clipPath.arcTo(new RectFloat(baseL, height - 2 * baseL, baseL * 3, height), 90, 90);
            clipPath.lineTo(baseL, baseL * 3);
            clipPath.lineTo(baseL / 5, baseL * 24 / 10);
            clipPath.arcTo(new RectFloat(0, baseL * 3 / 2, baseL, baseL * 5 / 2), 126.87f, 106.26f);
            clipPath.lineTo(baseL, baseL);
            clipPath.arcTo(new RectFloat(baseL, 0, baseL * 3, baseL * 2), 180, 90);
        }
        clipPath.close();
        canvas.clipPath(clipPath, Canvas.ClipOp.INTERSECT);
        if (pixelMap != null) {
            canvas.drawPixelMapHolderRect(new PixelMapHolder(pixelMap), new RectFloat(0, 0,
                    pixelMap.getImageInfo().size.width, pixelMap.getImageInfo().size.height), new RectFloat(0, 0,
                    component.getWidth(), component.getHeight()), new Paint());
        }
    }

    @Override
    public boolean onEstimateSize(int widthEstimateConfig, int heightEstimateConfig) {
        int sizeW = EstimateSpec.getSize(widthEstimateConfig);
        int sizeH = EstimateSpec.getSize(heightEstimateConfig);
        if (pixelMap != null) {
            int measuredWidth = Math.min(sizeW, pixelMap.getImageInfo().size.width);
            int measuredHeight = Math.min((int) (0.5f
                    + measuredWidth * pixelMap.getImageInfo().size.height / pixelMap.getImageInfo().size.width), sizeH);
            setEstimatedSize(EstimateSpec.getSizeWithMode(measuredWidth, EstimateSpec.PRECISE),
                    EstimateSpec.getSizeWithMode(measuredHeight, EstimateSpec.PRECISE));
            return true;
        }
        return false;
    }
}
