# ChatKit

**本项目是基于开源项目ChatKit进行ohos化的移植和开发的，可以通过项目标签以及github地址（https://github.com/stfalcon-studio/ChatKit ）追踪到原项目版本**

#### 项目介绍

- 项目名称：ChatKit
- 所属系列：ohos的第三方组件适配移植
- 功能：提供一种UI聊天框架。具有多种样式，支持可定制化UI以及数据管理
- 项目移植状态：完成
- 调用差异：无
- 项目作者和维护人：hihope
- 联系方式：hihope@hoperun.com
- 原项目Doc地址：https://github.com/stfalcon-studio/ChatKit
- 原项目基线版本：v0.4.1
- 编程语言：Java
- 外部库依赖：无

#### 效果演示

<img src="screenshot/效果动画.gif"/>

#### 安装教程

方法一：

1. 编译三方库har包
2. 启动 DevEco Studio，将下载的har包，导入工程目录“entry->libs”下。

3. 在module级别下的build.gradle文件中添加依赖，在dependences标签中增加对libs目录下har包的引用。

```groovy
repositories {
    flatDir { dirs 'libs' }
}
dependencies {
    implementation fileTree(dir: 'libs', include: ['*.jar', '*.har'])
    implementation(name: 'ChatKit', ext: 'har')
	……
}
```

方法二：

1. 在工程的build.gradle的allprojects中，添加HAR所在的Maven仓地址
```
repositories {
    maven {
        url 'http://106.15.92.248:8081/repository/Releases/' 
    }
}
```
2. 在应用模块的build.gradle的dependencies闭包中，添加如下代码:
```
dependencies {
    implementation 'com.stfalcon.ohos:chatkit:1.0.0'
}
```

#### 使用说明

使用封装好的ListContainer类DialogsList，xml布局引入
```java
    <com.stfalcon.chatkit.dialogs.DialogsList
        ohos:id="$+id:dialogsList"
        ohos:width="match_parent"
        ohos:height="match_parent" />
```
使用dialogsAdapter设置相关属性
```java
    private void initAdapter() {
        super.dialogsAdapter = new DialogsListAdapter<>(super.imageLoader);
        super.dialogsAdapter.setItems(DialogsFixtures.getDialogs());

        super.dialogsAdapter.setOnDialogClickListener(this);
        super.dialogsAdapter.setOnDialogLongClickListener(this);

        dialogsList.setAdapter(super.dialogsAdapter);
    }
```
设置列表子项布局格式
```java
    MessageHolders holdersConfig = new MessageHolders()
                .setIncomingTextLayout(ResourceTable.Layout_item_custom_incoming_text_message)
                .setOutcomingTextLayout(ResourceTable.Layout_item_custom_outcoming_text_message)
                .setIncomingImageLayout(ResourceTable.Layout_item_custom_incoming_image_message)
                .setOutcomingImageLayout(ResourceTable.Layout_item_custom_outcoming_image_message);
	super.messagesAdapter = new MessagesListAdapter<>(super.senderId, holdersConfig, super.imageLoader);

```


#### 版本迭代


- v1.0.0

- 提供一种UI聊天框架。具有多种样式，支持可定制化UI以及数据管理



#### 版权和许可信息

```
  Copyright (C) 2017 stfalcon.com
  
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at
  
  https://github.com/stfalcon-studio/ChatKit/blob/master/LICENSE
  
  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
```
