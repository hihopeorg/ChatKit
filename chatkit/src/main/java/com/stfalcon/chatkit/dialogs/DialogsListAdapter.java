/******************************************************************************
 * Copyright 2016 stfalcon.com
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/

package com.stfalcon.chatkit.dialogs;

import ohos.agp.colors.RgbColor;
import ohos.agp.components.BaseItemProvider;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.Image;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.Text;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.utils.Color;

import com.stfalcon.chatkit.ResourceTable;
import com.stfalcon.chatkit.commons.ImageLoader;
import com.stfalcon.chatkit.commons.ViewHolder;
import com.stfalcon.chatkit.commons.models.IDialog;
import com.stfalcon.chatkit.commons.models.IMessage;
import com.stfalcon.chatkit.utils.DateFormatter;

import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import static ohos.agp.components.Component.HIDE;
import static ohos.agp.components.Component.VISIBLE;

/**
 * Adapter for {@link DialogsList}
 */
@SuppressWarnings("WeakerAccess")
public class DialogsListAdapter<DIALOG extends IDialog>
        extends BaseItemProvider/*<DialogsListAdapter.BaseDialogViewHolder>*/ {
    protected List<DIALOG> items = new ArrayList<>();
    private int itemLayoutId;
    private Class<? extends BaseDialogViewHolder> holderClass;
    private ImageLoader imageLoader;
    private OnDialogClickListener<DIALOG> onDialogClickListener;
    private OnDialogViewClickListener<DIALOG> onDialogViewClickListener;
    private OnDialogLongClickListener<DIALOG> onLongItemClickListener;
    private OnDialogViewLongClickListener<DIALOG> onDialogViewLongClickListener;
    private DialogListStyle dialogStyle;
    private DateFormatter.Formatter datesFormatter;

    /**
     * For default list item layout and view holder
     *
     * @param imageLoader image loading method
     */
    public DialogsListAdapter(ImageLoader imageLoader) {
        this(ResourceTable.Layout_item_dialog, DialogViewHolder.class, imageLoader);
    }

    /**
     * For custom list item layout and default view holder
     *
     * @param itemLayoutId custom list item resource id
     * @param imageLoader  image loading method
     */
    public DialogsListAdapter(int itemLayoutId, ImageLoader imageLoader) {
        this(itemLayoutId, DialogViewHolder.class, imageLoader);
    }

    /**
     * For custom list item layout and custom view holder
     *
     * @param itemLayoutId custom list item resource id
     * @param holderClass  custom view holder class
     * @param imageLoader  image loading method
     */
    public DialogsListAdapter(int itemLayoutId, Class<? extends BaseDialogViewHolder> holderClass,
                              ImageLoader imageLoader) {
        this.itemLayoutId = itemLayoutId;
        this.holderClass = holderClass;
        this.imageLoader = imageLoader;
    }

    public void onBindViewHolder(BaseDialogViewHolder holder, int position) {
        holder.setImageLoader(imageLoader);
        holder.setOnDialogClickListener(onDialogClickListener);
        holder.setOnDialogViewClickListener(onDialogViewClickListener);
        holder.setOnLongItemClickListener(onLongItemClickListener);
        holder.setOnDialogViewLongClickListener(onDialogViewLongClickListener);
        holder.setDatesFormatter(datesFormatter);
        holder.onBind(items.get(position));
    }

    @Override
    public Component getComponent(int i, Component component, ComponentContainer componentContainer) {
        BaseDialogViewHolder holder = null;
        if (component == null) {
            holder = onCreateViewHolder(componentContainer, 0);
            component = holder.itemView;
            component.setTag(holder);
        } else {
            holder = (BaseDialogViewHolder)component.getTag();
        }
        onBindViewHolder(holder, i);
        return component;
    }

    public BaseDialogViewHolder onCreateViewHolder(ComponentContainer parent, int viewType) {
        Component v = LayoutScatter.getInstance(parent.getContext()).parse(itemLayoutId, parent, false);
        //create view holder by reflation
        try {
            Constructor<? extends BaseDialogViewHolder> constructor = holderClass.getDeclaredConstructor(Component.class);
            constructor.setAccessible(true);
            BaseDialogViewHolder baseDialogViewHolder = constructor.newInstance(v);
            if (baseDialogViewHolder instanceof DialogViewHolder) {
                ((DialogViewHolder) baseDialogViewHolder).setDialogStyle(dialogStyle);
            }
            return baseDialogViewHolder;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * @return size of dialogs list
     */
    @Override
    public int getCount() {
        return items.size();
    }

    /**
     * remove item with id
     *
     * @param id dialog i
     */
    public void deleteById(String id) {
        for (int i = 0; i < items.size(); i++) {
            if (items.get(i).getId().equals(id)) {
                items.remove(i);
                notifyDataSetItemRemoved(i);
            }
        }
    }

    /**
     * Returns {@code true} if, and only if, dialogs count in adapter is non-zero.
     *
     * @return {@code true} if size is 0, otherwise {@code false}
     */
    public boolean isEmpty() {
        return items.isEmpty();
    }

    /**
     * clear dialogs list
     */
    public void clear() {
        if (items != null) {
            items.clear();
        }
        notifyDataChanged();
    }

    /**
     * Set dialogs list
     *
     * @param items dialogs list
     */
    public void setItems(List<DIALOG> items) {
        this.items = items;
        notifyDataChanged();
    }

    /**
     * Add dialogs items
     *
     * @param newItems new dialogs list
     */
    public void addItems(List<DIALOG> newItems) {
        if (newItems != null) {
            if (items == null) {
                items = new ArrayList<>();
            }
            int curSize = items.size();
            items.addAll(newItems);
            notifyDataSetItemRangeInserted(curSize, items.size());
        }
    }

    /**
     * Add dialog to the end of dialogs list
     *
     * @param dialog dialog item
     */
    public void addItem(DIALOG dialog) {
        items.add(dialog);
        notifyDataSetItemInserted(items.size() - 1);
    }

    /**
     * Add dialog to dialogs list
     *
     * @param dialog   dialog item
     * @param position position in dialogs list
     */
    public void addItem(int position, DIALOG dialog) {
        items.add(position, dialog);
        notifyDataSetItemInserted(position);
    }

    /**
     * Move an item
     *
     * @param fromPosition the actual position of the item
     * @param toPosition   the new position of the item
     */
    public void moveItem(int fromPosition, int toPosition) {
        DIALOG dialog = items.remove(fromPosition);
        items.add(toPosition, dialog);
        notifyDataSetItemRangeChanged(fromPosition, toPosition - fromPosition);
    }

    /**
     * Update dialog by position in dialogs list
     *
     * @param position position in dialogs list
     * @param item     new dialog item
     */
    public void updateItem(int position, DIALOG item) {
        if (items == null) {
            items = new ArrayList<>();
        }
        items.set(position, item);
        notifyDataSetItemChanged(position);
    }

    /**
     * Update dialog by dialog id
     *
     * @param item new dialog item
     */
    public void updateItemById(DIALOG item) {
        if (items == null) {
            items = new ArrayList<>();
        }
        for (int i = 0; i < items.size(); i++) {
            if (items.get(i).getId().equals(item.getId())) {
                items.set(i, item);
                notifyDataSetItemChanged(i);
                break;
            }
        }
    }

    /**
     * Upsert dialog in dialogs list or add it to then end of dialogs list
     *
     * @param item dialog item
     */
    public void upsertItem(DIALOG item) {
        boolean updated = false;
        for (int i = 0; i < items.size(); i++) {
            if (items.get(i).getId().equals(item.getId())) {
                items.set(i, item);
                notifyDataSetItemChanged(i);
                updated = true;
                break;
            }
        }
        if (!updated) {
            addItem(item);
        }
    }

    /**
     * Find an item by its id
     *
     * @param id the wanted item's id
     * @return the found item, or null
     */
    public DIALOG getItemById(String id) {
        if (items == null) {
            items = new ArrayList<>();
        }
        for (DIALOG item : items) {
            if (item.getId() == null && id == null) {
                return item;
            } else if (item.getId() != null && item.getId().equals(id)) {
                return item;
            }
        }
        return null;
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    /**
     * Update last message in dialog and swap item to top of list.
     *
     * @param dialogId Dialog ID
     * @param message  New message
     * @return false if dialog doesn't exist.
     */
    @SuppressWarnings("unchecked")
    public boolean updateDialogWithMessage(String dialogId, IMessage message) {
        boolean dialogExist = false;
        for (int i = 0; i < items.size(); i++) {
            if (items.get(i).getId().equals(dialogId)) {
                items.get(i).setLastMessage(message);
                notifyDataSetItemChanged(i);
                if (i != 0) {
                    Collections.swap(items, i, 0);
                    notifyDataSetItemRangeChanged(0, i);
                }
                dialogExist = true;
                break;
            }
        }
        return dialogExist;
    }

    /**
     * Sort dialog by last message date
     */
    public void sortByLastMessageDate() {
        Collections.sort(items, (o1, o2) -> {
            if (o1.getLastMessage().getCreatedAt().after(o2.getLastMessage().getCreatedAt())) {
                return -1;
            } else if (o1.getLastMessage().getCreatedAt().before(o2.getLastMessage().getCreatedAt())) {
                return 1;
            } else return 0;
        });
        notifyDataChanged();
    }

    /**
     * Sort items with rules of comparator
     *
     * @param comparator Comparator
     */
    public void sort(Comparator<DIALOG> comparator) {
        Collections.sort(items, comparator);
        notifyDataChanged();
    }

    /**
     * @return registered image loader
     */
    public ImageLoader getImageLoader() {
        return imageLoader;
    }

    /**
     * Register a callback to be invoked when image need to load.
     *
     * @param imageLoader image loading method
     */
    public void setImageLoader(ImageLoader imageLoader) {
        this.imageLoader = imageLoader;
    }

    /**
     * @return the item click callback.
     */
    public OnDialogClickListener getOnDialogClickListener() {
        return onDialogClickListener;
    }

    /**
     * Register a callback to be invoked when item is clicked.
     *
     * @param onDialogClickListener on click item callback
     */
    public void setOnDialogClickListener(OnDialogClickListener<DIALOG> onDialogClickListener) {
        this.onDialogClickListener = onDialogClickListener;
    }

    /**
     * @return the view click callback.
     */
    public OnDialogViewClickListener getOnDialogViewClickListener() {
        return onDialogViewClickListener;
    }

    /**
     * Register a callback to be invoked when dialog view is clicked.
     *
     * @param clickListener on click item callback
     */
    public void setOnDialogViewClickListener(OnDialogViewClickListener<DIALOG> clickListener) {
        this.onDialogViewClickListener = clickListener;
    }

    /**
     * @return on long click item callback
     */
    public OnDialogLongClickListener getOnLongItemClickListener() {
        return onLongItemClickListener;
    }

    /**
     * Register a callback to be invoked when item is long clicked.
     *
     * @param onLongItemClickListener on long click item callback
     */
    public void setOnDialogLongClickListener(OnDialogLongClickListener<DIALOG> onLongItemClickListener) {
        this.onLongItemClickListener = onLongItemClickListener;
    }

    /**
     * @return on view long click callback
     */
    public OnDialogViewLongClickListener<DIALOG> getOnDialogViewLongClickListener() {
        return onDialogViewLongClickListener;
    }

    /**
     * Register a callback to be invoked when item view is long clicked.
     *
     * @param clickListener on long click item callback
     */
    public void setOnDialogViewLongClickListener(OnDialogViewLongClickListener<DIALOG> clickListener) {
        this.onDialogViewLongClickListener = clickListener;
    }

    /**
     * Sets custom {@link DateFormatter.Formatter} for text representation of last message date.
     */
    public void setDatesFormatter(DateFormatter.Formatter datesFormatter) {
        this.datesFormatter = datesFormatter;
    }

    //TODO ability to set style programmatically
    void setStyle(DialogListStyle dialogStyle) {
        this.dialogStyle = dialogStyle;
    }

    /**
     * @return the position of a dialog in the dialogs list.
     */
    public int getDialogPosition(DIALOG dialog) {
        return this.items.indexOf(dialog);
    }

    /*
     * LISTENERS
     * */
    public interface OnDialogClickListener<DIALOG extends IDialog> {
        void onDialogClick(DIALOG dialog);
    }

    public interface OnDialogViewClickListener<DIALOG extends IDialog> {
        void onDialogViewClick(Component view, DIALOG dialog);
    }

    public interface OnDialogLongClickListener<DIALOG extends IDialog> {
        void onDialogLongClick(DIALOG dialog);
    }

    public interface OnDialogViewLongClickListener<DIALOG extends IDialog> {
        void onDialogViewLongClick(Component view, DIALOG dialog);
    }

    /*
     * HOLDERS
     * */
    public abstract static class BaseDialogViewHolder<DIALOG extends IDialog>
            extends ViewHolder<DIALOG> {

        protected ImageLoader imageLoader;
        protected OnDialogClickListener<DIALOG> onDialogClickListener;
        protected OnDialogLongClickListener<DIALOG> onLongItemClickListener;
        protected OnDialogViewClickListener<DIALOG> onDialogViewClickListener;
        protected OnDialogViewLongClickListener<DIALOG> onDialogViewLongClickListener;
        protected DateFormatter.Formatter datesFormatter;

        public BaseDialogViewHolder(Component itemView) {
            super(itemView);
        }

        void setImageLoader(ImageLoader imageLoader) {
            this.imageLoader = imageLoader;
        }

        protected void setOnDialogClickListener(OnDialogClickListener<DIALOG> onDialogClickListener) {
            this.onDialogClickListener = onDialogClickListener;
        }

        protected void setOnDialogViewClickListener(OnDialogViewClickListener<DIALOG> onDialogViewClickListener) {
            this.onDialogViewClickListener = onDialogViewClickListener;
        }

        protected void setOnLongItemClickListener(OnDialogLongClickListener<DIALOG> onLongItemClickListener) {
            this.onLongItemClickListener = onLongItemClickListener;
        }

        protected void setOnDialogViewLongClickListener(OnDialogViewLongClickListener<DIALOG> onDialogViewLongClickListener) {
            this.onDialogViewLongClickListener = onDialogViewLongClickListener;
        }

        public void setDatesFormatter(DateFormatter.Formatter dateHeadersFormatter) {
            this.datesFormatter = dateHeadersFormatter;
        }
    }

    public static class DialogViewHolder<DIALOG extends IDialog> extends BaseDialogViewHolder<DIALOG> {
        protected DialogListStyle dialogStyle;
        protected ComponentContainer container;
        protected ComponentContainer root;
        protected Text tvName;
        protected Text tvDate;
        protected Image ivAvatar;
        protected Image ivLastMessageUser;
        protected Text tvLastMessage;
        protected Text tvBubble;
        protected ComponentContainer dividerContainer;
        protected Component divider;

        public DialogViewHolder(Component itemView) {
            super(itemView);
            root = (ComponentContainer) itemView.findComponentById(ResourceTable.Id_dialogRootLayout);
            container = (ComponentContainer) itemView.findComponentById(ResourceTable.Id_dialogContainer);
            tvName = (Text) itemView.findComponentById(ResourceTable.Id_dialogName);
            tvDate = (Text) itemView.findComponentById(ResourceTable.Id_dialogDate);
            tvLastMessage = (Text) itemView.findComponentById(ResourceTable.Id_dialogLastMessage);
            tvBubble = (Text) itemView.findComponentById(ResourceTable.Id_dialogUnreadBubble);
            ivLastMessageUser = (Image) itemView.findComponentById(ResourceTable.Id_dialogLastMessageUserAvatar);
            ivAvatar = (Image) itemView.findComponentById(ResourceTable.Id_dialogAvatar);
            dividerContainer = (ComponentContainer) itemView.findComponentById(ResourceTable.Id_dialogDividerContainer);
            divider = itemView.findComponentById(ResourceTable.Id_dialogDivider);
        }

        private void applyStyle() {
            if (dialogStyle != null) {
                //Texts
                if (tvName != null) {
                    tvName.setTextSize(dialogStyle.getDialogTitleTextSize(), Text.TextSizeType.PX);
                }

                if (tvLastMessage != null) {
                    tvLastMessage.setTextSize(dialogStyle.getDialogMessageTextSize(), Text.TextSizeType.PX);
                }

                if (tvDate != null) {
                    tvDate.setTextSize(dialogStyle.getDialogDateSize(), Text.TextSizeType.PX);
                }

                //Divider
                if (divider != null) {
                    ShapeElement backgroundElement = new ShapeElement();
                    backgroundElement.setRgbColor(RgbColor.fromArgbInt(dialogStyle.getDialogDividerColor()));
                    divider.setBackground(backgroundElement);
                }
                if (dividerContainer != null)
                    dividerContainer.setPadding(dialogStyle.getDialogDividerLeftPadding(), 0,
                            dialogStyle.getDialogDividerRightPadding(), 0);
                //Avatar
                if (ivAvatar != null) {
                    ivAvatar.getLayoutConfig().width = dialogStyle.getDialogAvatarWidth();
                    ivAvatar.getLayoutConfig().height = dialogStyle.getDialogAvatarHeight();
                }

                //Last message user avatar
                if (ivLastMessageUser != null) {
                    ivLastMessageUser.getLayoutConfig().width = dialogStyle.getDialogMessageAvatarWidth();
                    ivLastMessageUser.getLayoutConfig().height = dialogStyle.getDialogMessageAvatarHeight();
                }

                //Unread bubble
                if (tvBubble != null) {
                    ShapeElement bgShape = (ShapeElement) tvBubble.getBackgroundElement();
                    bgShape.setRgbColor(RgbColor.fromArgbInt(dialogStyle.getDialogUnreadBubbleBackgroundColor()));
                    tvBubble.setVisibility(dialogStyle.isDialogDividerEnabled() ? VISIBLE : HIDE);
                    tvBubble.setTextSize(dialogStyle.getDialogUnreadBubbleTextSize(), Text.TextSizeType.PX);
                    tvBubble.setTextColor(new Color(dialogStyle.getDialogUnreadBubbleTextColor()));
                    tvBubble.setFont(dialogStyle.makeFont(dialogStyle.getDialogUnreadBubbleTextStyle()));
                }
            }
        }


        private void applyDefaultStyle() {
            if (dialogStyle != null) {
                if (root != null) {
                    ShapeElement backgroundElement = new ShapeElement();
                    backgroundElement.setRgbColor(RgbColor.fromArgbInt(dialogStyle.getDialogItemBackground()));
                    root.setBackground(backgroundElement);
                }

                if (tvName != null) {
                    tvName.setTextColor(new Color(dialogStyle.getDialogTitleTextColor()));
                    tvName.setFont(dialogStyle.makeFont(dialogStyle.getDialogTitleTextStyle()));
                }

                if (tvDate != null) {
                    tvDate.setTextColor(new Color(dialogStyle.getDialogDateColor()));
                    tvDate.setFont(dialogStyle.makeFont(dialogStyle.getDialogDateStyle()));
                }

                if (tvLastMessage != null) {
                    tvLastMessage.setTextColor(new Color(dialogStyle.getDialogMessageTextColor()));
                    tvLastMessage.setFont(dialogStyle.makeFont(dialogStyle.getDialogMessageTextStyle()));
                }
            }
        }

        private void applyUnreadStyle() {
            if (dialogStyle != null) {
                if (root != null) {
                    ShapeElement backgroundElement = new ShapeElement();
                    backgroundElement.setRgbColor(RgbColor.fromArgbInt(dialogStyle.getDialogUnreadItemBackground()));
                    root.setBackground(backgroundElement);
                }

                if (tvName != null) {
                    tvName.setTextColor(new Color(dialogStyle.getDialogUnreadTitleTextColor()));
                    tvName.setFont(dialogStyle.makeFont(dialogStyle.getDialogUnreadTitleTextStyle()));
                }

                if (tvDate != null) {
                    tvDate.setTextColor(new Color(dialogStyle.getDialogUnreadDateColor()));
                    tvDate.setFont(dialogStyle.makeFont(dialogStyle.getDialogUnreadDateStyle()));
                }

                if (tvLastMessage != null) {
                    tvLastMessage.setTextColor(new Color(dialogStyle.getDialogUnreadMessageTextColor()));
                    tvLastMessage.setFont(dialogStyle.makeFont(dialogStyle.getDialogUnreadMessageTextStyle()));
                }
            }
        }


        @Override
        public void onBind(final DIALOG dialog) {
            if (dialog.getUnreadCount() > 0) {
                applyUnreadStyle();
            } else {
                applyDefaultStyle();
            }

            //Set Name
            tvName.setText(dialog.getDialogName());

            //Set Date
            String formattedDate = null;

            if (dialog.getLastMessage() != null) {
                Date lastMessageDate = dialog.getLastMessage().getCreatedAt();
                if (datesFormatter != null) formattedDate = datesFormatter.format(lastMessageDate);
                tvDate.setText(formattedDate == null
                        ? getDateString(lastMessageDate)
                        : formattedDate);
            } else {
                tvDate.setText(null);
            }

            //Set Dialog avatar
            if (imageLoader != null) {
                imageLoader.loadImage(ivAvatar, dialog.getDialogPhoto(), null);
            }

            //Set Last message user avatar with check if there is last message
            if (imageLoader != null && dialog.getLastMessage() != null) {
                imageLoader.loadImage(ivLastMessageUser, dialog.getLastMessage().getUser().getAvatar(), null);
            }
            ivLastMessageUser.setVisibility(dialogStyle.isDialogMessageAvatarEnabled()
                    && dialog.getUsers().size() > 1
                    && dialog.getLastMessage() != null ? VISIBLE : HIDE);

            //Set Last message text
            if (dialog.getLastMessage() != null) {
                tvLastMessage.setText(dialog.getLastMessage().getText());
            } else {
                tvLastMessage.setText(null);
            }

            //Set Unread message count bubble
            tvBubble.setText(String.valueOf(dialog.getUnreadCount()));
            tvBubble.setVisibility(dialogStyle.isDialogUnreadBubbleEnabled() &&
                    dialog.getUnreadCount() > 0 ? VISIBLE : HIDE);

            container.setClickedListener(view -> {
                if (onDialogClickListener != null) {
                    onDialogClickListener.onDialogClick(dialog);
                }
                if (onDialogViewClickListener != null) {
                    onDialogViewClickListener.onDialogViewClick(view, dialog);
                }
            });


            container.setLongClickedListener(view -> {
                if (onLongItemClickListener != null) {
                    onLongItemClickListener.onDialogLongClick(dialog);
                }
                if (onDialogViewLongClickListener != null) {
                    onDialogViewLongClickListener.onDialogViewLongClick(view, dialog);
                }
            });
        }

        protected String getDateString(Date date) {
            return DateFormatter.format(date, DateFormatter.Template.TIME);
        }

        protected DialogListStyle getDialogStyle() {
            return dialogStyle;
        }

        protected void setDialogStyle(DialogListStyle dialogStyle) {
            this.dialogStyle = dialogStyle;
            applyStyle();
        }
    }
}
