/*******************************************************************************
 * Copyright 2016 stfalcon.com
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/

package com.stfalcon.chatkit.dialogs;

import ohos.agp.components.AttrSet;
import ohos.agp.components.BaseItemProvider;
import ohos.agp.components.Component;
import ohos.agp.components.DirectionalLayoutManager;
import ohos.agp.components.ListContainer;
import ohos.app.Context;

import com.stfalcon.chatkit.commons.models.IDialog;

import static java.security.AccessController.getContext;

/**
 * Component for displaying list of dialogs
 */
public class DialogsList extends ListContainer implements Component.BindStateChangedListener {

    private DialogListStyle dialogStyle;

    public DialogsList(Context context) {
        super(context);
        init();
    }

    public DialogsList(Context context, AttrSet attrs) {
        super(context, attrs);
        parseStyle(context, attrs);
        init();
    }

    public DialogsList(Context context, AttrSet attrs, String defStyle) {
        super(context, attrs, defStyle);
        parseStyle(context, attrs);
        init();
    }

    private void init() {
        setBindStateChangedListener(this);
        setLongClickable(false);
    }

    @Override
    public void onComponentBoundToWindow(Component component) {

    }

    @Override
    public void onComponentUnboundFromWindow(Component component) {

    }

    /**
     * Don't use this method for setting your adapter, otherwise exception will by thrown.
     * Call {@link #setAdapter(DialogsListAdapter)} instead.
     */
    @Override
    public void setItemProvider(BaseItemProvider adapter) {
        throw new IllegalArgumentException("You can't set adapter to DialogsList. Use #setAdapter(DialogsListAdapter) instead.");
    }

    /**
     * Sets adapter for DialogsList
     *
     * @param adapter  Adapter. Must extend DialogsListAdapter
     * @param <DIALOG> Dialog model class
     */
    public <DIALOG extends IDialog<?>>
    void setAdapter(DialogsListAdapter<DIALOG> adapter) {
        setAdapter(adapter, false);
    }

    /**
     * Sets adapter for DialogsList
     *
     * @param adapter       Adapter. Must extend DialogsListAdapter
     * @param reverseLayout weather to use reverse layout for layout manager.
     * @param <DIALOG>      Dialog model class
     */
    public <DIALOG extends IDialog<?>>
    void setAdapter(DialogsListAdapter<DIALOG> adapter, boolean reverseLayout) {

        DirectionalLayoutManager layoutManager = new DirectionalLayoutManager();
        layoutManager.setOrientation(Component.VERTICAL);
        setLayoutManager(layoutManager);

        adapter.setStyle(dialogStyle);

        super.setItemProvider(adapter);
    }

    @SuppressWarnings("ResourceType")
    private void parseStyle(Context context, AttrSet attrs) {
        dialogStyle = DialogListStyle.parse(context, attrs);
    }
}
