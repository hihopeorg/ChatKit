/*******************************************************************************
 * Copyright 2016 stfalcon.com
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/

package com.stfalcon.chatkit.dialogs;

import ohos.agp.components.AttrSet;
import ohos.app.Context;

import com.stfalcon.chatkit.ResourceTable;
import com.stfalcon.chatkit.commons.Style;
import com.stfalcon.chatkit.utils.ResourceHelper;

/**
 * Style for DialogList customization by xml attributes
 */
@SuppressWarnings("WeakerAccess")
class DialogListStyle extends Style {

    private int dialogTitleTextColor;
    private int dialogTitleTextSize;
    private int dialogTitleTextStyle;
    private int dialogUnreadTitleTextColor;
    private int dialogUnreadTitleTextStyle;

    private int dialogMessageTextColor;
    private int dialogMessageTextSize;
    private int dialogMessageTextStyle;
    private int dialogUnreadMessageTextColor;
    private int dialogUnreadMessageTextStyle;

    private int dialogDateColor;
    private int dialogDateSize;
    private int dialogDateStyle;
    private int dialogUnreadDateColor;
    private int dialogUnreadDateStyle;

    private boolean dialogUnreadBubbleEnabled;
    private int dialogUnreadBubbleTextColor;
    private int dialogUnreadBubbleTextSize;
    private int dialogUnreadBubbleTextStyle;
    private int dialogUnreadBubbleBackgroundColor;

    private int dialogAvatarWidth;
    private int dialogAvatarHeight;

    private boolean dialogMessageAvatarEnabled;
    private int dialogMessageAvatarWidth;
    private int dialogMessageAvatarHeight;

    private boolean dialogDividerEnabled;
    private int dialogDividerColor;
    private int dialogDividerLeftPadding;
    private int dialogDividerRightPadding;

    private int dialogItemBackground;
    private int dialogUnreadItemBackground;

    static DialogListStyle parse(Context context, AttrSet attrs) {
        DialogListStyle style = new DialogListStyle(context, attrs);

        //Item background
        style.dialogItemBackground = ResourceHelper.getColorV(attrs, "dialogItemBackground",
                style.getColor(ResourceTable.Color_transparent));
        style.dialogUnreadItemBackground = ResourceHelper.getColorV(attrs, "dialogUnreadItemBackground",
                style.getColor(ResourceTable.Color_transparent));

        //Title text
        style.dialogTitleTextColor = ResourceHelper.getColorV(attrs, "dialogTitleTextColor",
                style.getColor(ResourceTable.Color_dialog_title_text));
        style.dialogTitleTextSize = ResourceHelper.getDimension(attrs, "dialogTitleTextSize",
                ResourceHelper.getDimensionFromFp(ResourceTable.Float_dialog_title_text_size_fp, context, 0));
        style.dialogTitleTextStyle = parseFontStyle(ResourceHelper.getString(attrs, "dialogTitleTextStyle", null));

        //Title unread text
        style.dialogUnreadTitleTextColor = ResourceHelper.getColorV(attrs, "dialogUnreadTitleTextColor",
                style.getColor(ResourceTable.Color_dialog_title_text));
        style.dialogUnreadTitleTextStyle = parseFontStyle(ResourceHelper.getString(attrs, "dialogUnreadTitleTextStyle", null));

        //Message text
        style.dialogMessageTextColor = ResourceHelper.getColorV(attrs, "dialogMessageTextColor",
                style.getColor(ResourceTable.Color_dialog_message_text));
        style.dialogMessageTextSize = ResourceHelper.getDimension(attrs, "dialogMessageTextSize",
                ResourceHelper.getDimensionFromFp(ResourceTable.Float_dialog_message_text_size_fp, context, 0));
        style.dialogMessageTextStyle = parseFontStyle(ResourceHelper.getString(attrs, "dialogMessageTextStyle", null));

        //Message unread text
        style.dialogUnreadMessageTextColor = ResourceHelper.getColorV(attrs, "dialogUnreadMessageTextColor",
                style.getColor(ResourceTable.Color_dialog_message_text));
        style.dialogUnreadMessageTextStyle = parseFontStyle(ResourceHelper.getString(attrs, "dialogUnreadMessageTextStyle", null));

        //Date text
        style.dialogDateColor = ResourceHelper.getColorV(attrs, "dialogDateColor",
                style.getColor(ResourceTable.Color_dialog_date_text));
        style.dialogDateSize = ResourceHelper.getDimension(attrs, "dialogDateSize",
                ResourceHelper.getDimensionFromFp(ResourceTable.Float_dialog_date_text_size_fp, context, 0));
        style.dialogDateStyle = parseFontStyle(ResourceHelper.getString(attrs, "dialogDateStyle", null));

        //Date unread text
        style.dialogUnreadDateColor = ResourceHelper.getColorV(attrs, "dialogUnreadDateColor",
                style.getColor(ResourceTable.Color_dialog_date_text));
        style.dialogUnreadDateStyle = parseFontStyle(ResourceHelper.getString(attrs, "dialogUnreadDateStyle", null));

        //Unread bubble
        style.dialogUnreadBubbleEnabled = ResourceHelper.getBool(attrs, "dialogUnreadBubbleEnabled", true);
        style.dialogUnreadBubbleBackgroundColor = ResourceHelper.getColorV(attrs, "dialogUnreadBubbleBackgroundColor",
                style.getColor(ResourceTable.Color_dialog_unread_bubble));

        //Unread bubble text
        style.dialogUnreadBubbleTextColor = ResourceHelper.getColorV(attrs, "dialogUnreadBubbleTextColor",
                style.getColor(ResourceTable.Color_dialog_unread_text));
        style.dialogUnreadBubbleTextSize = ResourceHelper.getDimension(attrs, "dialogUnreadBubbleTextSize",
                ResourceHelper.getDimensionFromFp(ResourceTable.Float_dialog_unread_bubble_text_size_fp, context, 0));
        style.dialogUnreadBubbleTextStyle = parseFontStyle(ResourceHelper.getString(attrs, "dialogUnreadBubbleTextStyle", null));

        //Avatar
        style.dialogAvatarWidth = ResourceHelper.getDimension(attrs, "dialogAvatarWidth",
                ResourceHelper.getDimensionFromVp(ResourceTable.Float_dialog_avatar_width_vp, context, 0));
        style.dialogAvatarHeight = ResourceHelper.getDimension(attrs, "dialogAvatarHeight",
                ResourceHelper.getDimensionFromVp(ResourceTable.Float_dialog_avatar_height_vp, context, 0));

        //Last message avatar
        style.dialogMessageAvatarEnabled = ResourceHelper.getBool(attrs, "dialogMessageAvatarEnabled", true);
        style.dialogMessageAvatarWidth = ResourceHelper.getDimension(attrs, "dialogMessageAvatarWidth",
                ResourceHelper.getDimensionFromVp(ResourceTable.Float_dialog_last_message_avatar_width_vp, context, 0));
        style.dialogMessageAvatarHeight = ResourceHelper.getDimension(attrs, "dialogMessageAvatarHeight",
                ResourceHelper.getDimensionFromVp(ResourceTable.Float_dialog_last_message_avatar_height_vp, context, 0));

        //Divider
        style.dialogDividerEnabled =  ResourceHelper.getBool(attrs, "dialogDividerEnabled", true);
        style.dialogDividerColor = ResourceHelper.getColorV(attrs, "dialogDividerColor", style.getColor(ResourceTable.Color_dialog_divider));
        style.dialogDividerLeftPadding = ResourceHelper.getDimension(attrs, "dialogDividerLeftPadding",
                ResourceHelper.getDimensionFromVp(ResourceTable.Float_dialog_divider_margin_left_vp, context, 0));
        style.dialogDividerRightPadding = ResourceHelper.getDimension(attrs, "dialogDividerRightPadding",
                ResourceHelper.getDimensionFromVp(ResourceTable.Float_dialog_divider_margin_right_vp, context, 0));

        return style;
    }

    private DialogListStyle(Context context, AttrSet attrs) {
        super(context, attrs);
    }

    protected int getDialogTitleTextColor() {
        return dialogTitleTextColor;
    }

    protected int getDialogTitleTextSize() {
        return dialogTitleTextSize;
    }

    protected int getDialogTitleTextStyle() {
        return dialogTitleTextStyle;
    }

    protected int getDialogUnreadTitleTextColor() {
        return dialogUnreadTitleTextColor;
    }

    protected int getDialogUnreadTitleTextStyle() {
        return dialogUnreadTitleTextStyle;
    }

    protected int getDialogMessageTextColor() {
        return dialogMessageTextColor;
    }

    protected int getDialogMessageTextSize() {
        return dialogMessageTextSize;
    }

    protected int getDialogMessageTextStyle() {
        return dialogMessageTextStyle;
    }

    protected int getDialogUnreadMessageTextColor() {
        return dialogUnreadMessageTextColor;
    }

    protected int getDialogUnreadMessageTextStyle() {
        return dialogUnreadMessageTextStyle;
    }

    protected int getDialogDateColor() {
        return dialogDateColor;
    }

    protected int getDialogDateSize() {
        return dialogDateSize;
    }

    protected int getDialogDateStyle() {
        return dialogDateStyle;
    }

    protected int getDialogUnreadDateColor() {
        return dialogUnreadDateColor;
    }

    protected int getDialogUnreadDateStyle() {
        return dialogUnreadDateStyle;
    }

    protected boolean isDialogUnreadBubbleEnabled() {
        return dialogUnreadBubbleEnabled;
    }

    protected int getDialogUnreadBubbleTextColor() {
        return dialogUnreadBubbleTextColor;
    }

    protected int getDialogUnreadBubbleTextSize() {
        return dialogUnreadBubbleTextSize;
    }

    protected int getDialogUnreadBubbleTextStyle() {
        return dialogUnreadBubbleTextStyle;
    }

    protected int getDialogUnreadBubbleBackgroundColor() {
        return dialogUnreadBubbleBackgroundColor;
    }

    protected int getDialogAvatarWidth() {
        return dialogAvatarWidth;
    }

    protected int getDialogAvatarHeight() {
        return dialogAvatarHeight;
    }

    protected boolean isDialogDividerEnabled() {
        return dialogDividerEnabled;
    }

    protected int getDialogDividerColor() {
        return dialogDividerColor;
    }

    protected int getDialogDividerLeftPadding() {
        return dialogDividerLeftPadding;
    }

    protected int getDialogDividerRightPadding() {
        return dialogDividerRightPadding;
    }

    protected int getDialogItemBackground() {
        return dialogItemBackground;
    }

    protected int getDialogUnreadItemBackground() {
        return dialogUnreadItemBackground;
    }

    protected boolean isDialogMessageAvatarEnabled() {
        return dialogMessageAvatarEnabled;
    }

    protected int getDialogMessageAvatarWidth() {
        return dialogMessageAvatarWidth;
    }

    protected int getDialogMessageAvatarHeight() {
        return dialogMessageAvatarHeight;
    }
}
