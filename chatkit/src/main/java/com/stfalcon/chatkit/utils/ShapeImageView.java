/*******************************************************************************
 * Copyright 2016 stfalcon.com
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/

package com.stfalcon.chatkit.utils;

import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.Image;
import ohos.agp.components.element.Element;
import ohos.agp.components.element.PixelMapElement;
import ohos.agp.render.Canvas;
import ohos.agp.render.Path;
import ohos.agp.render.PixelMapHolder;
import ohos.agp.utils.Point;
import ohos.app.Context;
import ohos.global.resource.NotExistException;
import ohos.media.image.PixelMap;

import java.io.IOException;

/**
 * ImageView with mask what described with Bézier Curves
 */

public class ShapeImageView extends Image implements Component.LayoutRefreshedListener, Component.DrawTask {
    private Path path;
    private int oldW;
    private int oldH;
    private Element element = null;

    public ShapeImageView(Context context) {
        super(context);
        init();
    }

    public ShapeImageView(Context context, AttrSet attrs) {
        super(context, attrs);
        init();
    }

    private void init() {
        setLayoutRefreshedListener(this);
        addDrawTask(this);
        oldH = getHeight();
        oldW = getWidth();
        if (oldW > 0 && oldH > 0) {
            initPath(oldW, oldH);
        }
    }

    @Override
    public void onRefreshed(Component component) {
        int h = component.getHeight();
        int w = component.getWidth();
        if (h != oldH || w != oldW) {
            initPath(w, h);
        }
        oldH = h;
        oldW = w;
    }

    private void initPath(int w, int h) {
        path = new Path();
        float halfWidth = (float) w / 2f;
        float firstParam = (float) w * 0.1f;
        float secondParam = (float) w * 0.8875f;

        //Bézier Curves
        path.moveTo(halfWidth, (float) w);
        path.cubicTo(new Point(firstParam, (float) w), new Point(0, secondParam), new Point(0, halfWidth));
        path.cubicTo(new Point(0, firstParam), new Point(firstParam, 0), new Point(halfWidth, 0));
        path.cubicTo(new Point(secondParam, 0), new Point((float) w, firstParam), new Point((float) w, halfWidth));
        path.cubicTo(new Point((float) w, secondParam), new Point(secondParam, (float) w), new Point(halfWidth,
                (float) w));
        path.close();
    }

    @Override
    public void onDraw(Component component, Canvas canvas) {
        if (path == null || path.isEmpty()) {
            if (element != null) {
                int saveCount = canvas.save();
                if (element.getHeight() > 0 && element.getWidth() > 0) {
                    //make image stretch
                    canvas.scale(
                            1f * component.getWidth() / element.getWidth(),
                            1f * component.getHeight() / element.getHeight());
                }
                element.drawToCanvas(canvas);
                canvas.restoreToCount(saveCount);
            }
            return;
        }

        int saveCount = canvas.save();
        canvas.clipPath(path, Canvas.ClipOp.INTERSECT);

        if (element != null) {
            if (element.getHeight() > 0 && element.getWidth() > 0) {
                //make image stretch
                canvas.scale(
                        1f * component.getWidth() / element.getWidth(),
                        1f * component.getHeight() / element.getHeight());
            }
            element.drawToCanvas(canvas);

        }
        canvas.restoreToCount(saveCount);
    }

    @Override
    public void setPixelMap(int resId) {
        try {
            element = new PixelMapElement(getContext().getResourceManager().getResource(resId));
        } catch (IOException e) {
            e.printStackTrace();
        } catch (NotExistException e) {
            e.printStackTrace();
        }
        invalidate();
    }

    @Override
    public void setPixelMap(PixelMap pixelMap) {
        element = new PixelMapElement(pixelMap);
        invalidate();
    }

    @Override
    public void setPixelMapHolder(PixelMapHolder pixelMapHolder) {
        element = new PixelMapElement(pixelMapHolder.getPixelMap());
        invalidate();
    }

    @Override
    public void setImageElement(Element element) {
        this.element = element;
        invalidate();
    }
}
