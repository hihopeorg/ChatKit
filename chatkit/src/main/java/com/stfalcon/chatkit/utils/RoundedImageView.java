package com.stfalcon.chatkit.utils;

import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.Image;
import ohos.app.Context;
import ohos.media.image.PixelMap;

/**
 * Thanks to Joonho Kim (https://github.com/pungrue26) for his lightweight SelectableRoundedImageView,
 * that was used as default image message representation
 */
public class RoundedImageView extends Image implements Component.EstimateSizeListener {

    private float[] mRadii = new float[]{0, 0, 0, 0, 0, 0, 0, 0};

    public RoundedImageView(Context context) {
        super(context);
    }

    public RoundedImageView(Context context, AttrSet attrs) {
        super(context, attrs);
        setEstimateSizeListener(this);
    }

    public RoundedImageView(Context context, AttrSet attrs, String defStyle) {
        super(context, attrs, defStyle);
    }

    public void setCorners(int leftTop, int rightTop,
                           int rightBottom, int leftBottom) {
        setCorners(
                (float) (leftTop == 0 ? 0 : ResourceHelper.getDimensionFromVp(leftTop, getContext(), 0)),
                (float) (rightTop == 0 ? 0 : ResourceHelper.getDimensionFromVp(rightTop, getContext(), 0)),
                (float) (rightBottom == 0 ? 0 : ResourceHelper.getDimensionFromVp(rightBottom, getContext(), 0)),
                (float) (leftBottom == 0 ? 0 : ResourceHelper.getDimensionFromVp(leftBottom, getContext(), 0))
        );
    }

    public void setCorners(float leftTop, float rightTop, float rightBottom, float leftBottom) {
        mRadii = new float[]{
                leftTop, leftTop,
                rightTop, rightTop,
                rightBottom, rightBottom,
                leftBottom, leftBottom};
        super.setCornerRadii(mRadii);
    }

    @Override
    public boolean onEstimateSize(int widthEstimateConfig,
                                  int heightEstimateConfig) {
        int modeW = EstimateSpec.getMode(widthEstimateConfig);
        int sizeW = EstimateSpec.getSize(widthEstimateConfig);
        int modeH = EstimateSpec.getMode(heightEstimateConfig);
        int sizeH = EstimateSpec.getSize(heightEstimateConfig);
        PixelMap pixelMap = getPixelMap();
        if (pixelMap != null) {
            int measuredWidth = Math.min(sizeW, pixelMap.getImageInfo().size.width);
            int measuredHeight = Math.min((int) (0.5f
                    + measuredWidth * pixelMap.getImageInfo().size.height / pixelMap.getImageInfo().size.width), sizeH);
            setEstimatedSize(EstimateSpec.getSizeWithMode(measuredWidth, EstimateSpec.PRECISE),
                    EstimateSpec.getSizeWithMode(measuredHeight, EstimateSpec.PRECISE));
            return true;
        }
        return false;
    }
}
