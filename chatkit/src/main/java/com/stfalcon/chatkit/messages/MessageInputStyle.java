/*******************************************************************************
 * Copyright 2016 stfalcon.com
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/

package com.stfalcon.chatkit.messages;

import ohos.agp.components.AttrSet;
import ohos.agp.components.ComponentState;
import ohos.agp.components.element.Element;
import ohos.agp.render.BlendMode;
import ohos.app.Context;

import com.stfalcon.chatkit.ResourceTable;
import com.stfalcon.chatkit.commons.Style;
import com.stfalcon.chatkit.utils.ResourceHelper;

/**
 * Style for MessageInputStyle customization by xml attributes
 */
@SuppressWarnings("WeakerAccess")
class MessageInputStyle extends Style {

    private static final int DEFAULT_MAX_LINES = 5;
    private static final int DEFAULT_DELAY_TYPING_STATUS = 1500;

    private boolean showAttachmentButton;

    private int attachmentButtonBackground;
    private int attachmentButtonDefaultBgColor;
    private int attachmentButtonDefaultBgPressedColor;
    private int attachmentButtonDefaultBgDisabledColor;

    private int attachmentButtonIcon;
    private int attachmentButtonDefaultIconColor;
    private int attachmentButtonDefaultIconPressedColor;
    private int attachmentButtonDefaultIconDisabledColor;

    private int attachmentButtonWidth;
    private int attachmentButtonHeight;
    private int attachmentButtonMargin;

    private int inputButtonBackground;
    private int inputButtonDefaultBgColor;
    private int inputButtonDefaultBgPressedColor;
    private int inputButtonDefaultBgDisabledColor;

    private int inputButtonIcon;
    private int inputButtonDefaultIconColor;
    private int inputButtonDefaultIconPressedColor;
    private int inputButtonDefaultIconDisabledColor;

    private int inputButtonWidth;
    private int inputButtonHeight;
    private int inputButtonMargin;

    private int inputMaxLines;
    private String inputHint;
    private String inputText;

    private int inputTextSize;
    private int inputTextColor;
    private int inputHintColor;

    private Element inputBackground;
    private Element inputCursorDrawable;

    private int inputDefaultPaddingLeft;
    private int inputDefaultPaddingRight;
    private int inputDefaultPaddingTop;
    private int inputDefaultPaddingBottom;

    private int delayTypingStatus;

    static MessageInputStyle parse(Context context, AttrSet attrs) {
        MessageInputStyle style = new MessageInputStyle(context, attrs);

        style.showAttachmentButton = ResourceHelper.getBool(attrs, "showAttachmentButton", false);

        style.attachmentButtonBackground = ResourceHelper.getResourceId(attrs, "attachmentButtonBackground", -1);
        style.attachmentButtonDefaultBgColor = ResourceHelper.getColorV(attrs, "attachmentButtonDefaultBgColor",
                style.getColor(ResourceTable.Color_white_four));
        style.attachmentButtonDefaultBgPressedColor = ResourceHelper.getColorV(attrs, "attachmentButtonDefaultBgPressedColor",
                style.getColor(ResourceTable.Color_white_five));
        style.attachmentButtonDefaultBgDisabledColor = ResourceHelper.getColorV(attrs, "attachmentButtonDefaultBgDisabledColor",
                style.getColor(ResourceTable.Color_transparent));

        style.attachmentButtonIcon = ResourceHelper.getResourceId(attrs, "attachmentButtonIcon", -1);
        style.attachmentButtonDefaultIconColor = ResourceHelper.getColorV(attrs, "attachmentButtonDefaultIconColor",
                style.getColor(ResourceTable.Color_cornflower_blue_two));
        style.attachmentButtonDefaultIconPressedColor = ResourceHelper.getColorV(attrs, "attachmentButtonDefaultIconPressedColor",
                style.getColor(ResourceTable.Color_cornflower_blue_two_dark));
        style.attachmentButtonDefaultIconDisabledColor = ResourceHelper.getColorV(attrs, "attachmentButtonDefaultIconDisabledColor",
                style.getColor(ResourceTable.Color_cornflower_blue_light_40));

        style.attachmentButtonWidth = ResourceHelper.getDimension(attrs, "attachmentButtonWidth", style.getDimensionFromVp(ResourceTable.Float_input_button_width_vp));
        style.attachmentButtonHeight = ResourceHelper.getDimension(attrs, "attachmentButtonHeight", style.getDimensionFromVp(ResourceTable.Float_input_button_height_vp));
        style.attachmentButtonMargin = ResourceHelper.getDimension(attrs, "attachmentButtonMargin", style.getDimensionFromVp(ResourceTable.Float_input_button_margin_vp));

        style.inputButtonBackground = ResourceHelper.getResourceId(attrs, "inputButtonBackground", -1);
        style.inputButtonDefaultBgColor = ResourceHelper.getColorV(attrs, "inputButtonDefaultBgColor",
                style.getColor(ResourceTable.Color_cornflower_blue_two));
        style.inputButtonDefaultBgPressedColor = ResourceHelper.getColorV(attrs, "inputButtonDefaultBgPressedColor",
                style.getColor(ResourceTable.Color_cornflower_blue_two_dark));
        style.inputButtonDefaultBgDisabledColor = ResourceHelper.getColorV(attrs, "inputButtonDefaultBgDisabledColor",
                style.getColor(ResourceTable.Color_white_four));

        style.inputButtonIcon = ResourceHelper.getResourceId(attrs, "inputButtonIcon", -1);
        style.inputButtonDefaultIconColor = ResourceHelper.getColorV(attrs, "inputButtonDefaultIconColor",
                style.getColor(ResourceTable.Color_white));
        style.inputButtonDefaultIconPressedColor = ResourceHelper.getColorV(attrs, "inputButtonDefaultIconPressedColor",
                style.getColor(ResourceTable.Color_white));
        style.inputButtonDefaultIconDisabledColor = ResourceHelper.getColorV(attrs, "inputButtonDefaultIconDisabledColor",
                style.getColor(ResourceTable.Color_warm_grey));

        style.inputButtonWidth = ResourceHelper.getDimension(attrs, "inputButtonWidth", style.getDimensionFromVp(ResourceTable.Float_input_button_width_vp));
        style.inputButtonHeight = ResourceHelper.getDimension(attrs, "inputButtonHeight", style.getDimensionFromVp(ResourceTable.Float_input_button_height_vp));
        style.inputButtonMargin = ResourceHelper.getDimension(attrs, "inputButtonMargin", style.getDimensionFromVp(ResourceTable.Float_input_button_margin_vp));

        style.inputMaxLines = ResourceHelper.getInt(attrs, "inputMaxLines", DEFAULT_MAX_LINES);
        style.inputHint = ResourceHelper.getString(attrs, "inputHint", null);
        style.inputText = ResourceHelper.getString(attrs, "inputText", null);

        style.inputTextSize = ResourceHelper.getDimension(attrs, "inputTextSize", style.getDimensionFromFp(ResourceTable.Float_input_text_size_fp));
        style.inputTextColor = ResourceHelper.getColorV(attrs, "inputTextColor", style.getColor(ResourceTable.Color_dark_grey_two));
        style.inputHintColor = ResourceHelper.getColorV(attrs, "inputHintColor", style.getColor(ResourceTable.Color_warm_grey_three));

        style.inputBackground = ResourceHelper.getElement(attrs, "inputBackground", null);
        style.inputCursorDrawable = ResourceHelper.getElement(attrs, "inputCursorDrawable", null);

        style.delayTypingStatus = ResourceHelper.getInt(attrs, "delayTypingStatus", DEFAULT_DELAY_TYPING_STATUS);

        style.inputDefaultPaddingLeft = style.getDimensionFromFp(ResourceTable.Float_input_padding_left_fp);
        style.inputDefaultPaddingRight = style.getDimensionFromFp(ResourceTable.Float_input_padding_right_fp);
        style.inputDefaultPaddingTop = style.getDimensionFromFp(ResourceTable.Float_input_padding_top_fp);
        style.inputDefaultPaddingBottom = style.getDimensionFromFp(ResourceTable.Float_input_padding_bottom_fp);

        return style;
    }

    private MessageInputStyle(Context context, AttrSet attrs) {
        super(context, attrs);
    }

    private Element getSelector(int normalColor, int pressedColor,
                                 int disabledColor, int shape) {
        Element drawable = getVectorDrawable(shape);
        drawable.setStateColorList(new int[][]{
                        new int[]{-ComponentState.COMPONENT_STATE_DISABLED, -ComponentState.COMPONENT_STATE_PRESSED},
                        new int[]{-ComponentState.COMPONENT_STATE_DISABLED, ComponentState.COMPONENT_STATE_PRESSED},
                        new int[]{ComponentState.COMPONENT_STATE_DISABLED}
                },
                new int[]{normalColor, pressedColor, disabledColor});
        drawable.setStateColorMode(BlendMode.SRC_IN);
        return drawable;
    }

    protected boolean showAttachmentButton() {
        return showAttachmentButton;
    }

    protected Element getAttachmentButtonBackground() {
        if (attachmentButtonBackground == -1) {
            return getSelector(attachmentButtonDefaultBgColor, attachmentButtonDefaultBgPressedColor,
                    attachmentButtonDefaultBgDisabledColor, ResourceTable.Media_mask);
        } else {
            return getDrawable(attachmentButtonBackground);
        }
    }

    protected Element getAttachmentButtonIcon() {
        if (attachmentButtonIcon == -1) {
            return getSelector(attachmentButtonDefaultIconColor, attachmentButtonDefaultIconPressedColor,
                    attachmentButtonDefaultIconDisabledColor, ResourceTable.Media_ic_add_attachment);
        } else {
            return getDrawable(attachmentButtonIcon);
        }
    }

    protected int getAttachmentButtonWidth() {
        return attachmentButtonWidth;
    }

    protected int getAttachmentButtonHeight() {
        return attachmentButtonHeight;
    }

    protected int getAttachmentButtonMargin() {
        return attachmentButtonMargin;
    }

    protected Element getInputButtonBackground() {
        if (inputButtonBackground == -1) {
            return getSelector(inputButtonDefaultBgColor, inputButtonDefaultBgPressedColor,
                    inputButtonDefaultBgDisabledColor, ResourceTable.Media_mask);
        } else {
            return getDrawable(inputButtonBackground);
        }
    }

    protected Element getInputButtonIcon() {
        if (inputButtonIcon == -1) {
            return getSelector(inputButtonDefaultIconColor, inputButtonDefaultIconPressedColor,
                    inputButtonDefaultIconDisabledColor, ResourceTable.Media_ic_send);
        } else {
            return getDrawable(inputButtonIcon);
        }
    }

    protected int getInputButtonMargin() {
        return inputButtonMargin;
    }

    protected int getInputButtonWidth() {
        return inputButtonWidth;
    }

    protected int getInputButtonHeight() {
        return inputButtonHeight;
    }

    protected int getInputMaxLines() {
        return inputMaxLines;
    }

    protected String getInputHint() {
        return inputHint;
    }

    protected String getInputText() {
        return inputText;
    }

    protected int getInputTextSize() {
        return inputTextSize;
    }

    protected int getInputTextColor() {
        return inputTextColor;
    }

    protected int getInputHintColor() {
        return inputHintColor;
    }

    protected Element getInputBackground() {
        return inputBackground;
    }

    protected Element getInputCursorDrawable() {
        return inputCursorDrawable;
    }

    protected int getInputDefaultPaddingLeft() {
        return inputDefaultPaddingLeft;
    }

    protected int getInputDefaultPaddingRight() {
        return inputDefaultPaddingRight;
    }

    protected int getInputDefaultPaddingTop() {
        return inputDefaultPaddingTop;
    }

    protected int getInputDefaultPaddingBottom() {
        return inputDefaultPaddingBottom;
    }

    int getDelayTypingStatus() {
        return delayTypingStatus;
    }

}
