/*******************************************************************************
 * Copyright 2016 stfalcon.com
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/

package com.stfalcon.chatkit.messages;

import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.DependentLayout;
import ohos.agp.components.Image;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.Text;
import ohos.agp.components.TextField;
import ohos.agp.components.element.Element;
import ohos.agp.components.element.PixelMapElement;
import ohos.agp.utils.Color;
import ohos.app.Context;
import ohos.app.dispatcher.task.Revocable;

//import com.stfalcon.chatkit.R;
import com.stfalcon.chatkit.ResourceTable;

import java.lang.reflect.Field;

/**
 * Component for input outcoming messages
 */
@SuppressWarnings({"WeakerAccess", "unused"})
public class MessageInput extends DependentLayout
        implements Component.ClickedListener, Text.TextObserver, Component.FocusChangedListener {

    protected TextField messageInput;
    protected Image messageSendButton;
    protected Image attachmentButton;
    protected Component sendButtonSpace, attachmentButtonSpace;

    private String input;
    private InputListener inputListener;
    private AttachmentsListener attachmentsListener;
    private boolean isTyping;
    private TypingListener typingListener;
    private int delayTypingStatusMillis;
    private Runnable typingTimerRunnable = new Runnable() {
        @Override
        public void run() {
            typingTimerRevocable = null;
            if (isTyping) {
                isTyping = false;
                if (typingListener != null) typingListener.onStopTyping();
            }
        }
    };
    Revocable typingTimerRevocable;
    private boolean lastFocus;

    public MessageInput(Context context) {
        super(context);
        init(context);
    }

    public MessageInput(Context context, AttrSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public MessageInput(Context context, AttrSet attrs, String defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    /**
     * Sets callback for 'submit' button.
     *
     * @param inputListener input callback
     */
    public void setInputListener(InputListener inputListener) {
        this.inputListener = inputListener;
    }

    /**
     * Sets callback for 'add' button.
     *
     * @param attachmentsListener input callback
     */
    public void setAttachmentsListener(AttachmentsListener attachmentsListener) {
        this.attachmentsListener = attachmentsListener;
    }

    /**
     * Returns EditText for messages input
     *
     * @return EditText
     */
    public TextField getInputEditText() {
        return messageInput;
    }

    /**
     * Returns `submit` button
     *
     * @return ImageButton
     */
    public Image getButton() {
        return messageSendButton;
    }

    @Override
    public void onClick(Component view) {
        int id = view.getId();
        if (id == ResourceTable.Id_messageSendButton) {
            boolean isSubmitted = onSubmit();
            if (isSubmitted) {
                messageInput.setText("");
            }
            if (typingTimerRevocable != null) {
                typingTimerRevocable.revoke();
                typingTimerRevocable = null;
            }
            typingTimerRevocable = getContext().getUITaskDispatcher().asyncDispatch(typingTimerRunnable);
        } else if (id == ResourceTable.Id_attachmentButton) {
            onAddAttachments();
        }
    }

    /**
     * This method is called to notify you that, within s,
     * the count characters beginning at start have just replaced old text that had length before
     */
    @Override
    public void onTextUpdated(String s, int start, int before, int count) {
        input = s;
        messageSendButton.setEnabled(input.length() > 0);
        messageSendButton.setClickable(input.length() > 0);
        if (s.length() > 0) {
            if (!isTyping) {
                isTyping = true;
                if (typingListener != null) typingListener.onStartTyping();
            }
            if (typingTimerRevocable != null) {
                typingTimerRevocable.revoke();
                typingTimerRevocable = null;
            }
            typingTimerRevocable = getContext().getUITaskDispatcher().delayDispatch(typingTimerRunnable, delayTypingStatusMillis);
        }
    }

    @Override
    public void onFocusChange(Component v, boolean hasFocus) {
        if (lastFocus && !hasFocus && typingListener != null) {
            typingListener.onStopTyping();
        }
        lastFocus = hasFocus;
    }

    private boolean onSubmit() {
        return inputListener != null && inputListener.onSubmit(input);
    }

    private void onAddAttachments() {
        if (attachmentsListener != null) attachmentsListener.onAddAttachments();
    }

    private void init(Context context, AttrSet attrs) {
        init(context);
        MessageInputStyle style = MessageInputStyle.parse(context, attrs);

        this.messageInput.setMaxTextLines(style.getInputMaxLines());
        this.messageInput.setHint(style.getInputHint());
        this.messageInput.setText(style.getInputText());
        this.messageInput.setTextSize(style.getInputTextSize(), Text.TextSizeType.PX);
        this.messageInput.setTextColor(new Color(style.getInputTextColor()));
        this.messageInput.setHintColor(new Color(style.getInputHintColor()));
        this.messageInput.setBackground(style.getInputBackground());
        setCursor(style.getInputCursorDrawable());
        // workaround: ohos do not support set left_of / right_of constraint at the same time, so set a fixed right_margin
        this.messageInput.setMarginRight(style.getInputButtonWidth() + style.getInputButtonMargin());

        this.attachmentButton.setVisibility(style.showAttachmentButton() ? VISIBLE : HIDE);
        this.attachmentButton.setWidth(style.getAttachmentButtonWidth());
        this.attachmentButton.setHeight(style.getAttachmentButtonHeight());
        setImageElement(this.attachmentButton, style.getAttachmentButtonIcon());
        this.attachmentButton.setBackground(style.getAttachmentButtonBackground());

        this.attachmentButtonSpace.setVisibility(style.showAttachmentButton() ? VISIBLE : HIDE);
        this.attachmentButtonSpace.setWidth(style.getAttachmentButtonMargin());

        this.messageSendButton.setWidth(style.getInputButtonWidth());
        this.messageSendButton.setHeight(style.getInputButtonHeight());
        setImageElement(this.messageSendButton, style.getInputButtonIcon());
        messageSendButton.setBackground(style.getInputButtonBackground());
        this.sendButtonSpace.setWidth(style.getInputButtonMargin());

        if (getPaddingLeft() == 0
                && getPaddingRight() == 0
                && getPaddingTop() == 0
                && getPaddingBottom() == 0) {
            setPadding(
                    style.getInputDefaultPaddingLeft(),
                    style.getInputDefaultPaddingTop(),
                    style.getInputDefaultPaddingRight(),
                    style.getInputDefaultPaddingBottom()
            );
        }
        this.delayTypingStatusMillis = style.getDelayTypingStatus();
    }

    private void setImageElement(Image image, Element element) {
        if (element instanceof PixelMapElement) {
            // workaround: setPixelMap() doesn't support StateColorList, setImageElement() doesn't support ScaleMode
            // use Element.setBounds() to make it display in center
            int halfOffsetH = (image.getWidth() - element.getWidth()) / 2;
            int halfOffsetV = (image.getHeight() - element.getHeight()) / 2;
            element.setBounds(halfOffsetH, halfOffsetV,
                    image.getWidth() - halfOffsetH, image.getHeight() - halfOffsetV);
        }
        image.setImageElement(element);
    }

    private void init(Context context) {
        LayoutScatter.getInstance(context).parse(ResourceTable.Layout_view_message_input, this, true);

        messageInput = (TextField) findComponentById(ResourceTable.Id_messageInput);
        messageSendButton = (Image) findComponentById(ResourceTable.Id_messageSendButton);
        attachmentButton = (Image) findComponentById(ResourceTable.Id_attachmentButton);
        sendButtonSpace = findComponentById(ResourceTable.Id_sendButtonSpace);
        attachmentButtonSpace = findComponentById(ResourceTable.Id_attachmentButtonSpace);

        messageSendButton.setEnabled(false);
        messageSendButton.setClickable(false);
        messageSendButton.setClickedListener(this);
        attachmentButton.setClickedListener(this);
        messageInput.addTextObserver(this);
        messageInput.setText("");
        messageInput.setFocusChangedListener(this);
    }

    private void setCursor(Element element) {
        if (element == null) return;
        this.messageInput.setCursorElement(element);
    }

    public void setTypingListener(TypingListener typingListener) {
        this.typingListener = typingListener;
    }

    /**
     * Interface definition for a callback to be invoked when user pressed 'submit' button
     */
    public interface InputListener {

        /**
         * Fires when user presses 'send' button.
         *
         * @param input input entered by user
         * @return if input text is valid, you must return {@code true} and input will be cleared, otherwise return false.
         */
        boolean onSubmit(CharSequence input);
    }

    /**
     * Interface definition for a callback to be invoked when user presses 'add' button
     */
    public interface AttachmentsListener {

        /**
         * Fires when user presses 'add' button.
         */
        void onAddAttachments();
    }

    /**
     * Interface definition for a callback to be invoked when user typing
     */
    public interface TypingListener {

        /**
         * Fires when user presses start typing
         */
        void onStartTyping();

        /**
         * Fires when user presses stop typing
         */
        void onStopTyping();

    }
}
