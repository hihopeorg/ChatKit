/*******************************************************************************
 * Copyright 2016 stfalcon.com
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/

package com.stfalcon.chatkit.messages;

import ohos.agp.components.AttrSet;
import ohos.agp.components.BaseItemProvider;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.DirectionalLayoutManager;
import ohos.agp.components.ListContainer;
import ohos.app.Context;

import com.stfalcon.chatkit.commons.models.IMessage;

/**
 * Component for displaying list of messages
 */
public class MessagesList extends ListContainer implements ComponentContainer.ArrangeListener {
    private MessagesListStyle messagesListStyle;
    private RecyclerScrollMoreListener recyclerScrollMoreListener;

    public MessagesList(Context context) {
        super(context);
        init();
    }

    public MessagesList(Context context, AttrSet attrs) {
        super(context, attrs);
        parseStyle(context, attrs);
        init();
    }

    public MessagesList(Context context, AttrSet attrs, String defStyle) {
        super(context, attrs, defStyle);
        parseStyle(context, attrs);
        init();
    }

    private void init() {
        setArrangeListener(this);
        setLongClickable(false);
    }

    /**
     * Don't use this method for setting your adapter, otherwise exception will by thrown.
     * Call {@link #setAdapter(MessagesListAdapter)} instead.
     */
    @Override
    public void setItemProvider(BaseItemProvider adapter) {
        throw new IllegalArgumentException("You can't set adapter to MessagesList. Use #setAdapter(MessagesListAdapter) instead.");
    }

    /**
     * Sets adapter for MessagesList
     *
     * @param adapter   Adapter. Must extend MessagesListAdapter
     * @param <MESSAGE> Message model class
     */
    public <MESSAGE extends IMessage>
    void setAdapter(MessagesListAdapter<MESSAGE> adapter) {
        setAdapter(adapter, true);
    }

    /**
     * Sets adapter for MessagesList
     *
     * @param adapter       Adapter. Must extend MessagesListAdapter
     * @param reverseLayout weather to use reverse layout for layout manager.
     * @param <MESSAGE>     Message model class
     */
    public <MESSAGE extends IMessage>
    void setAdapter(MessagesListAdapter<MESSAGE> adapter, boolean reverseLayout) {
        //reverseLayout no use for now

        DirectionalLayoutManager layoutManager = new DirectionalLayoutManager();
        layoutManager.setOrientation(Component.VERTICAL);

        setLayoutManager(layoutManager);
        adapter.setLayoutManager(layoutManager, this);
        adapter.setStyle(messagesListStyle);

        recyclerScrollMoreListener = new RecyclerScrollMoreListener(layoutManager, adapter);
        setScrolledListener(recyclerScrollMoreListener);
        super.setItemProvider(adapter);
    }

    @SuppressWarnings("ResourceType")
    private void parseStyle(Context context, AttrSet attrs) {
        messagesListStyle = MessagesListStyle.parse(context, attrs);
    }

    @Override
    public boolean onArrange(int i, int i1, int i2, int i3) {
        if (recyclerScrollMoreListener != null) {
            recyclerScrollMoreListener.onContentScrolled(this, 0, 0, 0, 0);
        }
        return false;
    }
}
