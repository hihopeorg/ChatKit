/*******************************************************************************
 * Copyright 2016 stfalcon.com
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/

package com.stfalcon.chatkit.messages;

import ohos.agp.components.AttrSet;
import ohos.agp.components.ComponentState;
import ohos.agp.components.element.Element;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.render.BlendMode;
import ohos.agp.utils.Color;
import ohos.app.Context;

import com.stfalcon.chatkit.ResourceTable;
import com.stfalcon.chatkit.commons.Style;
import com.stfalcon.chatkit.utils.ResourceHelper;

/**
 * Style for MessagesListStyle customization by xml attributes
 */
@SuppressWarnings("WeakerAccess")
class MessagesListStyle extends Style {

    private int textAutoLinkMask;
    private int incomingTextLinkColor;
    private int outcomingTextLinkColor;

    private int incomingAvatarWidth;
    private int incomingAvatarHeight;

    private int incomingBubbleDrawable;
    private int incomingDefaultBubbleColor;
    private int incomingDefaultBubblePressedColor;
    private int incomingDefaultBubbleSelectedColor;

    private int incomingImageOverlayDrawable;
    private int incomingDefaultImageOverlayPressedColor;
    private int incomingDefaultImageOverlaySelectedColor;

    private int incomingDefaultBubblePaddingLeft;
    private int incomingDefaultBubblePaddingRight;
    private int incomingDefaultBubblePaddingTop;
    private int incomingDefaultBubblePaddingBottom;

    private int incomingTextColor;
    private int incomingTextSize;
    private int incomingTextStyle;

    private int incomingTimeTextColor;
    private int incomingTimeTextSize;
    private int incomingTimeTextStyle;

    private int incomingImageTimeTextColor;
    private int incomingImageTimeTextSize;
    private int incomingImageTimeTextStyle;

    private int outcomingBubbleDrawable;
    private int outcomingDefaultBubbleColor;
    private int outcomingDefaultBubblePressedColor;
    private int outcomingDefaultBubbleSelectedColor;

    private int outcomingImageOverlayDrawable;
    private int outcomingDefaultImageOverlayPressedColor;
    private int outcomingDefaultImageOverlaySelectedColor;

    private int outcomingDefaultBubblePaddingLeft;
    private int outcomingDefaultBubblePaddingRight;
    private int outcomingDefaultBubblePaddingTop;
    private int outcomingDefaultBubblePaddingBottom;

    private int outcomingTextColor;
    private int outcomingTextSize;
    private int outcomingTextStyle;

    private int outcomingTimeTextColor;
    private int outcomingTimeTextSize;
    private int outcomingTimeTextStyle;

    private int outcomingImageTimeTextColor;
    private int outcomingImageTimeTextSize;
    private int outcomingImageTimeTextStyle;

    private int dateHeaderPadding;
    private String dateHeaderFormat;
    private int dateHeaderTextColor;
    private int dateHeaderTextSize;
    private int dateHeaderTextStyle;

    static MessagesListStyle parse(Context context, AttrSet attrs) {
        MessagesListStyle style = new MessagesListStyle(context, attrs);

        style.textAutoLinkMask = ResourceHelper.getInt(attrs,"textAutoLink", 0);
        style.incomingTextLinkColor = ResourceHelper.getColorV(attrs,"incomingTextLinkColor",
                style.getSystemAccentColor());
        style.outcomingTextLinkColor = ResourceHelper.getColorV(attrs,"outcomingTextLinkColor",
                style.getSystemAccentColor());

        style.incomingAvatarWidth = ResourceHelper.getDimension(attrs,"incomingAvatarWidth",
                style.getDimensionFromVp(ResourceTable.Float_message_avatar_width_vp));
        style.incomingAvatarHeight = ResourceHelper.getDimension(attrs,"incomingAvatarHeight",
                style.getDimensionFromVp(ResourceTable.Float_message_avatar_height_vp));

        style.incomingBubbleDrawable = ResourceHelper.getResourceId(attrs,"incomingBubbleDrawable", -1);
        style.incomingDefaultBubbleColor = ResourceHelper.getColorV(attrs,"incomingDefaultBubbleColor",
                style.getColor(ResourceTable.Color_white_two));
        style.incomingDefaultBubblePressedColor = ResourceHelper.getColorV(attrs,"incomingDefaultBubblePressedColor",
                style.getColor(ResourceTable.Color_white_two));
        style.incomingDefaultBubbleSelectedColor = ResourceHelper.getColorV(attrs,"incomingDefaultBubbleSelectedColor",
                style.getColor(ResourceTable.Color_cornflower_blue_two_24));

        style.incomingImageOverlayDrawable = ResourceHelper.getResourceId(attrs,"incomingImageOverlayDrawable", -1);
        style.incomingDefaultImageOverlayPressedColor = ResourceHelper.getColorV(attrs,"incomingDefaultImageOverlayPressedColor",
                style.getColor(ResourceTable.Color_transparent));
        style.incomingDefaultImageOverlaySelectedColor = ResourceHelper.getColorV(attrs,"incomingDefaultImageOverlaySelectedColor",
                style.getColor(ResourceTable.Color_cornflower_blue_light_40));

        style.incomingDefaultBubblePaddingLeft = ResourceHelper.getDimension(attrs,"incomingBubblePaddingLeft",
                style.getDimensionFromVp(ResourceTable.Float_message_padding_left_vp));
        style.incomingDefaultBubblePaddingRight = ResourceHelper.getDimension(attrs,"incomingBubblePaddingRight",
                style.getDimensionFromVp(ResourceTable.Float_message_padding_right_vp));
        style.incomingDefaultBubblePaddingTop = ResourceHelper.getDimension(attrs,"incomingBubblePaddingTop",
                style.getDimensionFromVp(ResourceTable.Float_message_padding_top_vp));
        style.incomingDefaultBubblePaddingBottom = ResourceHelper.getDimension(attrs,"incomingBubblePaddingBottom",
                style.getDimensionFromVp(ResourceTable.Float_message_padding_bottom_vp));
        style.incomingTextColor = ResourceHelper.getColorV(attrs,"incomingTextColor",
                style.getColor(ResourceTable.Color_dark_grey_two));
        style.incomingTextSize = ResourceHelper.getDimension(attrs,"incomingTextSize",
                style.getDimensionFromFp(ResourceTable.Float_message_text_size_fp));
        style.incomingTextStyle = parseFontStyle(ResourceHelper.getString(attrs,"incomingTextStyle", null));

        style.incomingTimeTextColor = ResourceHelper.getColorV(attrs,"incomingTimeTextColor",
                style.getColor(ResourceTable.Color_warm_grey_four));
        style.incomingTimeTextSize = ResourceHelper.getDimension(attrs,"incomingTimeTextSize",
                style.getDimensionFromFp(ResourceTable.Float_message_time_text_size_fp));
        style.incomingTimeTextStyle = parseFontStyle(ResourceHelper.getString(attrs,"incomingTimeTextStyle", null));

        style.incomingImageTimeTextColor = ResourceHelper.getColorV(attrs,"incomingImageTimeTextColor",
                style.getColor(ResourceTable.Color_warm_grey_four));
        style.incomingImageTimeTextSize = ResourceHelper.getDimension(attrs,"incomingImageTimeTextSize",
                style.getDimensionFromFp(ResourceTable.Float_message_time_text_size_fp));
        style.incomingImageTimeTextStyle = parseFontStyle(ResourceHelper.getString(attrs,"incomingImageTimeTextStyle", null));

        style.outcomingBubbleDrawable = ResourceHelper.getResourceId(attrs,"outcomingBubbleDrawable", -1);
        style.outcomingDefaultBubbleColor = ResourceHelper.getColorV(attrs,"outcomingDefaultBubbleColor",
                style.getColor(ResourceTable.Color_cornflower_blue_two));
        style.outcomingDefaultBubblePressedColor = ResourceHelper.getColorV(attrs,"outcomingDefaultBubblePressedColor",
                style.getColor(ResourceTable.Color_cornflower_blue_two));
        style.outcomingDefaultBubbleSelectedColor = ResourceHelper.getColorV(attrs,"outcomingDefaultBubbleSelectedColor",
                style.getColor(ResourceTable.Color_cornflower_blue_two_24));

        style.outcomingImageOverlayDrawable = ResourceHelper.getResourceId(attrs,"outcomingImageOverlayDrawable", -1);
        style.outcomingDefaultImageOverlayPressedColor = ResourceHelper.getColorV(attrs,"outcomingDefaultImageOverlayPressedColor",
                style.getColor(ResourceTable.Color_transparent));
        style.outcomingDefaultImageOverlaySelectedColor = ResourceHelper.getColorV(attrs,"outcomingDefaultImageOverlaySelectedColor",
                style.getColor(ResourceTable.Color_cornflower_blue_light_40));

        style.outcomingDefaultBubblePaddingLeft = ResourceHelper.getDimension(attrs,"outcomingBubblePaddingLeft",
                style.getDimensionFromVp(ResourceTable.Float_message_padding_left_vp));
        style.outcomingDefaultBubblePaddingRight = ResourceHelper.getDimension(attrs,"outcomingBubblePaddingRight",
                style.getDimensionFromVp(ResourceTable.Float_message_padding_right_vp));
        style.outcomingDefaultBubblePaddingTop = ResourceHelper.getDimension(attrs,"outcomingBubblePaddingTop",
                style.getDimensionFromVp(ResourceTable.Float_message_padding_top_vp));
        style.outcomingDefaultBubblePaddingBottom = ResourceHelper.getDimension(attrs,"outcomingBubblePaddingBottom",
                style.getDimensionFromVp(ResourceTable.Float_message_padding_bottom_vp));
        style.outcomingTextColor = ResourceHelper.getColorV(attrs,"outcomingTextColor",
                style.getColor(ResourceTable.Color_white));
        style.outcomingTextSize = ResourceHelper.getDimension(attrs,"outcomingTextSize",
                style.getDimensionFromFp(ResourceTable.Float_message_text_size_fp));
        style.outcomingTextStyle = parseFontStyle(ResourceHelper.getString(attrs,"outcomingTextStyle", null));

        style.outcomingTimeTextColor = ResourceHelper.getColorV(attrs,"outcomingTimeTextColor",
                style.getColor(ResourceTable.Color_white60));
        style.outcomingTimeTextSize = ResourceHelper.getDimension(attrs,"outcomingTimeTextSize",
                style.getDimensionFromFp(ResourceTable.Float_message_time_text_size_fp));
        style.outcomingTimeTextStyle = parseFontStyle(ResourceHelper.getString(attrs,"outcomingTimeTextStyle", null));

        style.outcomingImageTimeTextColor = ResourceHelper.getColorV(attrs,"outcomingImageTimeTextColor",
                style.getColor(ResourceTable.Color_warm_grey_four));
        style.outcomingImageTimeTextSize = ResourceHelper.getDimension(attrs,"outcomingImageTimeTextSize",
                style.getDimensionFromFp(ResourceTable.Float_message_time_text_size_fp));
        style.outcomingImageTimeTextStyle = parseFontStyle(ResourceHelper.getString(attrs,"outcomingImageTimeTextStyle", null));

        style.dateHeaderPadding = ResourceHelper.getDimension(attrs,"dateHeaderPadding",
                style.getDimensionFromVp(ResourceTable.Float_message_date_header_padding_vp));
        style.dateHeaderFormat = ResourceHelper.getString(attrs,"dateHeaderFormat", null);
        style.dateHeaderTextColor = ResourceHelper.getColorV(attrs,"dateHeaderTextColor",
                style.getColor(ResourceTable.Color_warm_grey_two));
        style.dateHeaderTextSize = ResourceHelper.getDimension(attrs,"dateHeaderTextSize",
                style.getDimensionFromFp(ResourceTable.Float_message_date_header_text_size_fp));
        style.dateHeaderTextStyle = parseFontStyle(ResourceHelper.getString(attrs,"dateHeaderTextStyle", null));
        return style;
    }

    private MessagesListStyle(Context context, AttrSet attrs) {
        super(context, attrs);
    }

    private Element getMessageSelector(int normalColor, int selectedColor,
                                       int pressedColor, int shape) {
        Element drawable = getVectorDrawable(shape);

        drawable.setStateColorList(new int[][]{
                new int[]{ComponentState.COMPONENT_STATE_SELECTED},
                new int[]{ComponentState.COMPONENT_STATE_PRESSED},
                new int[]{-ComponentState.COMPONENT_STATE_SELECTED, -ComponentState.COMPONENT_STATE_PRESSED},
        }, new int[] {selectedColor, pressedColor, normalColor});
        drawable.setStateColorMode(BlendMode.SRC_IN);

        // workaround for ohos not support set different corners in xml
        int corner = ResourceHelper.getDimensionFromVp(ResourceTable.Float_message_bubble_corners_radius_vp, context, 0);
        if (shape == ResourceTable.Graphic_shape_incoming_message) {
            ((ShapeElement) drawable).setCornerRadiiArray(new float[]{corner, corner, corner, corner, corner, corner,
                    0, 0});
        } else if (shape == ResourceTable.Graphic_shape_outcoming_message) {
            ((ShapeElement) drawable).setCornerRadiiArray(new float[]{corner, corner, corner, corner, 0, 0, corner, corner});
        }
        return drawable;
    }

    protected int getTextAutoLinkMask() {
        return textAutoLinkMask;
    }

    protected int getIncomingTextLinkColor() {
        return incomingTextLinkColor;
    }

    protected int getOutcomingTextLinkColor() {
        return outcomingTextLinkColor;
    }

    protected int getIncomingAvatarWidth() {
        return incomingAvatarWidth;
    }

    protected int getIncomingAvatarHeight() {
        return incomingAvatarHeight;
    }

    protected int getIncomingDefaultBubblePaddingLeft() {
        return incomingDefaultBubblePaddingLeft;
    }

    protected int getIncomingDefaultBubblePaddingRight() {
        return incomingDefaultBubblePaddingRight;
    }

    protected int getIncomingDefaultBubblePaddingTop() {
        return incomingDefaultBubblePaddingTop;
    }

    protected int getIncomingDefaultBubblePaddingBottom() {
        return incomingDefaultBubblePaddingBottom;
    }

    protected int getIncomingTextColor() {
        return incomingTextColor;
    }

    protected int getIncomingTextSize() {
        return incomingTextSize;
    }

    protected int getIncomingTextStyle() {
        return incomingTextStyle;
    }

    protected Element getOutcomingBubbleDrawable() {
        if (outcomingBubbleDrawable == -1) {
            return getMessageSelector(outcomingDefaultBubbleColor, outcomingDefaultBubbleSelectedColor,
                    outcomingDefaultBubblePressedColor, ResourceTable.Graphic_shape_outcoming_message);
        } else {
            return getDrawable(outcomingBubbleDrawable);
        }
    }

    protected Element getOutcomingImageOverlayDrawable() {
        if (outcomingImageOverlayDrawable == -1) {
            return getMessageSelector(Color.TRANSPARENT.getValue(), outcomingDefaultImageOverlaySelectedColor,
                    outcomingDefaultImageOverlayPressedColor, ResourceTable.Graphic_shape_outcoming_message);
        } else {
            return getDrawable(outcomingImageOverlayDrawable);
        }
    }

    protected int getOutcomingDefaultBubblePaddingLeft() {
        return outcomingDefaultBubblePaddingLeft;
    }

    protected int getOutcomingDefaultBubblePaddingRight() {
        return outcomingDefaultBubblePaddingRight;
    }

    protected int getOutcomingDefaultBubblePaddingTop() {
        return outcomingDefaultBubblePaddingTop;
    }

    protected int getOutcomingDefaultBubblePaddingBottom() {
        return outcomingDefaultBubblePaddingBottom;
    }

    protected int getOutcomingTextColor() {
        return outcomingTextColor;
    }

    protected int getOutcomingTextSize() {
        return outcomingTextSize;
    }

    protected int getOutcomingTextStyle() {
        return outcomingTextStyle;
    }

    protected int getOutcomingTimeTextColor() {
        return outcomingTimeTextColor;
    }

    protected int getOutcomingTimeTextSize() {
        return outcomingTimeTextSize;
    }

    protected int getOutcomingTimeTextStyle() {
        return outcomingTimeTextStyle;
    }

    protected int getOutcomingImageTimeTextColor() {
        return outcomingImageTimeTextColor;
    }

    protected int getOutcomingImageTimeTextSize() {
        return outcomingImageTimeTextSize;
    }

    protected int getOutcomingImageTimeTextStyle() {
        return outcomingImageTimeTextStyle;
    }

    protected int getDateHeaderTextColor() {
        return dateHeaderTextColor;
    }

    protected int getDateHeaderTextSize() {
        return dateHeaderTextSize;
    }

    protected int getDateHeaderTextStyle() {
        return dateHeaderTextStyle;
    }

    protected int getDateHeaderPadding() {
        return dateHeaderPadding;
    }

    protected String getDateHeaderFormat() {
        return dateHeaderFormat;
    }

    protected int getIncomingTimeTextSize() {
        return incomingTimeTextSize;
    }

    protected int getIncomingTimeTextStyle() {
        return incomingTimeTextStyle;
    }

    protected int getIncomingTimeTextColor() {
        return incomingTimeTextColor;
    }

    protected int getIncomingImageTimeTextColor() {
        return incomingImageTimeTextColor;
    }

    protected int getIncomingImageTimeTextSize() {
        return incomingImageTimeTextSize;
    }

    protected int getIncomingImageTimeTextStyle() {
        return incomingImageTimeTextStyle;
    }

    protected Element getIncomingBubbleDrawable() {
        if (incomingBubbleDrawable == -1) {
            return getMessageSelector(incomingDefaultBubbleColor, incomingDefaultBubbleSelectedColor,
                    incomingDefaultBubblePressedColor, ResourceTable.Graphic_shape_incoming_message);
        } else {
            return getDrawable(incomingBubbleDrawable);
        }
    }

    protected Element getIncomingImageOverlayDrawable() {
        if (incomingImageOverlayDrawable == -1) {
            return getMessageSelector(Color.TRANSPARENT.getValue(), incomingDefaultImageOverlaySelectedColor,
                    incomingDefaultImageOverlayPressedColor, ResourceTable.Graphic_shape_incoming_message);
        } else {
            return getDrawable(incomingImageOverlayDrawable);
        }
    }
}
