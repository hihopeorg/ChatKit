/*******************************************************************************
 * Copyright 2016 stfalcon.com
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/

package com.stfalcon.chatkit.messages;

import ohos.agp.components.BaseItemProvider;
import ohos.agp.components.Component;
import ohos.agp.components.DirectionalLayoutManager;
import ohos.agp.components.LayoutManager;
import ohos.agp.components.ListContainer;
import ohos.agp.components.TableLayoutManager;

class RecyclerScrollMoreListener
        implements Component.ScrolledListener {

    private OnLoadMoreListener loadMoreListener;
    private int currentPage = 0;
    private int previousTotalItemCount = 0;
    private boolean loading = true;

    private LayoutManager mLayoutManager;

    RecyclerScrollMoreListener(LayoutManager layoutManager, OnLoadMoreListener loadMoreListener) {
        this.mLayoutManager = layoutManager;
        this.loadMoreListener = loadMoreListener;
    }

    @Override
    public void onContentScrolled(Component component, int i, int i1, int i2, int i3) {
        if (!(component instanceof ListContainer)) {
            return;
        }

        ListContainer view = (ListContainer)component;
        BaseItemProvider itemProvider = ((ListContainer) component).getItemProvider();
        if (loadMoreListener != null) {
            int lastVisibleItemPosition = 0;
            int totalItemCount = itemProvider.getCount();

            if (mLayoutManager instanceof DirectionalLayoutManager || mLayoutManager instanceof TableLayoutManager) {
                lastVisibleItemPosition = view.getItemPosByVisibleIndex(view.getVisibleIndexCount() -1);
            }

            if (totalItemCount < previousTotalItemCount) {
                this.currentPage = 0;
                this.previousTotalItemCount = totalItemCount;
                if (totalItemCount == 0) {
                    this.loading = true;
                }
            }

            if (loading && (totalItemCount > previousTotalItemCount)) {
                loading = false;
                previousTotalItemCount = totalItemCount;
            }

            int visibleThreshold = 5;
            if (!loading && (lastVisibleItemPosition + visibleThreshold) > totalItemCount) {
                currentPage++;
                loadMoreListener.onLoadMore(loadMoreListener.getMessagesCount(), totalItemCount);
                loading = true;
            }
        }
    }

    interface OnLoadMoreListener {
        void onLoadMore(int page, int total);

        int getMessagesCount();
    }
}
