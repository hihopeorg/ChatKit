/*******************************************************************************
 * Copyright 2016 stfalcon.com
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/

package com.stfalcon.chatkit.commons;

import ohos.agp.components.AttrSet;
import ohos.agp.components.element.Element;
import ohos.agp.text.Font;
import ohos.agp.utils.TextTool;
import ohos.app.Context;
import ohos.global.systemres.ResourceTable;
import ohos.global.resource.ResourceManager;

import com.stfalcon.chatkit.utils.ResourceHelper;

/**
 * Base class for chat component styles
 */
public abstract class Style {
    public static final int NORMAL = 0;
    public static final int BOLD = 1;
    public static final int ITALIC = 2;
    public static final int BOLD_ITALIC = 3;

    protected Context context;
    protected ResourceManager resources;
    protected AttrSet attrs;

    protected Style(Context context, AttrSet attrs) {
        this.context = context;
        this.resources = context.getResourceManager();
        this.attrs = attrs;
    }

    protected final int getSystemAccentColor() {
        return getSystemColor(ResourceTable.Color_id_color_activated);
    }

    protected final int getSystemPrimaryColor() {
        return getSystemColor(ResourceTable.Color_id_color_primary);
    }

    protected final int getSystemPrimaryDarkColor() {
        return getSystemColor(ResourceTable.Color_id_color_primary_dark);
    }

    protected final int getSystemPrimaryTextColor() {
        return getSystemColor(ResourceTable.Color_id_color_text_primary);
    }

    protected final int getSystemHintColor() {
        return getSystemColor(ResourceTable.Color_id_color_text_hint);
    }

    protected final int getSystemColor(int attr) {
        return ResourceHelper.getColorV(attr, context, 0);
    }

    protected final int getDimension(int dimen) {
        return ResourceHelper.getDimensionFromPx(dimen, context, 0);
    }

    protected final int getDimensionFromVp(int dimen) {
        return ResourceHelper.getDimensionFromVp(dimen, context, 0);
    }

    protected final int getDimensionFromFp(int dimen) {
        return ResourceHelper.getDimensionFromFp(dimen, context, 0);
    }

    protected final int getColor(int color) {
        return context.getColor(color);
    }

    protected final Element getDrawable(int drawable) {
        return ResourceHelper.getElement(drawable, context, null);
    }

    protected final Element getVectorDrawable(int drawable) {
        return ResourceHelper.getElement(drawable, context, null);
    }

    protected static int parseFontStyle(String rawValue) {
        int result = 0;
        if (TextTool.isNullOrEmpty(rawValue)) {
            return result;
        }
        String[] values = rawValue.split("\\|");
        for (String value : values) {
            switch (value.trim()) {
                case "italic":
                    result |= ITALIC;
                    break;
                case "bold":
                    result |= BOLD;
                    break;
                default:
                    break;
            }
        }
        return result;
    }

    public Font makeFont(int style) {
        Font.Builder builder = new Font.Builder("sans-serif");
        builder.makeItalic((style & ITALIC) != 0);
        builder.setWeight((style & BOLD) != 0 ? Font.BOLD : Font.REGULAR);
        return builder.build();
    }
}
